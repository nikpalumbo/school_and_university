#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#define BUFFSIZE 4096
int main (int argc, char *argv[]) {
/*Dichiarazione di variabili*/
int n,bytes;//Intero utilizzato per tener traccia del numero di caratteri letti dalla funzione read
char buff[BUFFSIZE];//Buffer di lettura e scrittura
char cmd[BUFFSIZE];
int sock_fd;/*Intero positivo che indica il socket descriptor; � utilizzato successivamente da altre
funzioni come parametro per riferirsi alla socket di comunicazione tra client e server*/
struct sockaddr_in serv_addr; /*Struttura dati tipica del dominio AF_INET per la gesione dell'indirizzamento*/
int i;//Intero utilizzato nel ciclo for per la lettura della stringa di comando dallo STD_INP

if (argc != 2) {
printf("Errore nell'inserimento dei parametri\nUsage: %s [Indirizzo Server]\n", argv[0]);
exit(1);
}

if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
perror("Errore in fase di creazione di socket\n");
exit(1);
}

memset((void *) &serv_addr, 0, sizeof(serv_addr));
serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(2000);

if ((inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)) <= 0) {
perror("Errore in fase di creazione indirizzo\n");
exit(1);
}

printf("Connessione al Server....Attendere......\n\n");
if (connect(sock_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
perror("Errore in fase di connessione\n");
exit(1);
}

printf("Connessione riuscita...\nInserire il comando da eseguire : ");

for (i=0; (cmd[i]=getchar()) != '\n'; i++);
cmd[i]= '\0';

printf("Esecuzione in corso...Attesa di risposta dal Server\n\n");
write(sock_fd, cmd, BUFFSIZE);


while(1){
bytes=read(sock_fd,buff,BUFFSIZE);
if(bytes<=0)
 exit(0);
write(1,buff,bytes);
}
}
printf("Disconnessione in corso...\n");
close(sock_fd);
exit(0);
}






 
