#include <sys/types.h>
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <signal.h>
#define BUFFSIZE 7000
#define MAXCONN 5

void leggi(int conn_fd);


int main(void) {

int on=1; //Intero utilizzato per tener traccia del numero di caratteri letti dalla funzione read
int fd_sock, conn_fd; /*Intero positivo che indica il socket descriptor; fd_sock indica la socket del server
ed � utilizzato dalle funzioni bind, listen e accept; conn_fd � il socket file descriptor restituito dalla
funzione accept e consente la comunicazione diretta col client attualmente connesso */
struct sockaddr_in server,client; /*Struttura dati tipica del dominio AF_INET per la gesione dell'indirizzamento*/

if ((fd_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
perror("Errore : Creazione socket fallita");
exit(1);}
setsockopt(fd_sock,SOL_SOCKET,SO_REUSEADDR,(char*)&on,sizeof(on));

memset((void *)&server, 0, sizeof(server));

server.sin_family = AF_INET;
server.sin_port = htons(2000);
server.sin_addr.s_addr = htonl(INADDR_ANY);

if ((bind(fd_sock, (struct sockaddr *)&server, sizeof(server)) < 0)) {
      perror("Errore : Bind socket fallita");
      exit(1);}

if (listen(fd_sock, MAXCONN) < 0) {
       perror("Errore : Listen fallito");
       exit(1);}

signal(SIGCHLD,SIG_IGN);	 /*qs chiamata serve per evitare che il figlio */
				/* rimanga 'zombie' solo perche' deve restituire il
				   codice di ritorno al padre */


while(1){

    if ((conn_fd = accept(fd_sock, (struct sockaddr *) NULL, NULL)) < 0) {
       perror("Errore : Accept fallito");
       exit(1);
    }
    if (fork()==0){		/* figlio */
       leggi(conn_fd);
       exit(0);			/* figlio termina */
    } 
    else {
           close(conn_fd);		/* padre chiude sempre fd */
    }
}
}

void leggi(int conn_fd){
int n;
FILE *fd_pipe;
char buffer[BUFFSIZE], buffer2[BUFFSIZE]; //Buffer di lettura e scrittura

read(conn_fd, &buffer, BUFFSIZE);
    
	if ((fd_pipe = popen(buffer, "r")) == NULL) {
              perror("Errore : Apertura Pipe fallita");
              exit(1);
           }
         while ((n=fread(buffer2, 1, BUFFSIZE, fd_pipe)) > 0) {
             if (write(conn_fd,buffer2,n)!=n) {
                perror("Erorre : Scrittura Pipe fallita"); 
                exit(1);}
             }
         pclose(fd_pipe);
}
