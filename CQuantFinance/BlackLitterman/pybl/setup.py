# -*- coding: utf-8 -*-
""" pybl setup.py script """
import multiprocessing
# pybl
from pybl import __version__

# system
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
from os.path import join, dirname


setup(
    name='pybl',
    version='0.1.0',
    description='My Python implementation of Black Litterman Model',
    author='Nicola Palumbo',
    author_email='nikpalumbo@gmail.com',
    packages=['pybl', 'pybl.test'],
    url='http://meetfinance.net',
    long_description=open('README.md').read(),
    install_requires=[],
    test_suite='nose.collector',
    tests_require=['mock', 'nose', 'coverage'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
      ],
)
