import unittest
from pybl.asset import Asset
from pybl.view import View
from pybl.core import TimeSeries

class TestView(unittest.TestCase):

    def test_asset(self):
        """Test the __init__ of View"""
        name = 'My Name'
        description = 'My short description'
        confidence = 0.5
        p = [0, 1]
        q = [0, 1]

        asset1 = Asset('AAPL', 'EQ', 100,
                      TimeSeries([100.0, 101.0]))

        asset2 = Asset('GOOG', 'EQ', 80,
                      TimeSeries([10.0, 11.0]))
        assets = [asset1, asset2]

        view = View(name, description, confidence, p, q, assets)

        self.assertEqual(view.name, name)
        self.assertEqual(view.description, description)
        self.assertAlmostEqual(view.confidence, confidence, 1)
        self.assertEqual(view.p_array, p)
        self.assertEqual(view.q_array, q)
        self.assertIsInstance(view.p_array, list)
        self.assertIsInstance(view.q_array, list)
        self.assertIsInstance(view.assets, list)
        self.assertIsInstance(view.assets[0], Asset)


if __name__ == '__main__':
    unittest.main()