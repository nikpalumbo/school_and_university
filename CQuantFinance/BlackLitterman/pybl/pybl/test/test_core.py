import unittest
from pybl.core import TimeSeries


class TestTimeSeries(unittest.TestCase):

    def setUp(self):
        self.ts = TimeSeries((10, 20, 30))

    def test_getitem(self):
        """
        Test the getitem
        """
        self.assertEqual(self.ts[0], 10)

    def test_len(self):
        """
        Test the len
        """
        self.assertEqual(len(self.ts), 3)

    def test_size(self):
        """
        Tests the length of the time series.
        The result has to be a float
        """
        self.assertAlmostEqual(self.ts.size(), 3.0, 1)

    def test_mean(self):
        """
        Tests the mean.
        """
        self.assertEqual(self.ts.mean(), 20)

    def test_std(self):
        """
        Tests the standard deviation.
        """
        self.assertEqual(self.ts.std(), 8.1649658092772608)

    def test_variance(self):
        """
        Tests the variance.
        """
        self.assertEqual(self.ts.variance(), 66.666666666666671)

    def test_gain(self):
        """
        Tests the return compared with previous index.
        """
        self.assertEqual(self.ts.gain(0), 0)
        self.assertAlmostEqual(self.ts.gain(1), 1)

if __name__ == '__main__':
    unittest.main()