import unittest
from pybl.asset import Asset
from pybl.core import TimeSeries

class TestAsset(unittest.TestCase):

    def test_asset(self):
        """Test the __init__ of Asset."""
        mk_cap = 100
        a_id = 1
        ts = [9, 10, 11, 12]
        a_type = 'EQ'
        asset = Asset(a_id, a_type, mk_cap, ts)
        self.assertEqual(asset.asset_id, a_id)
        self.assertEqual(asset.market_cap, mk_cap)
        self.assertEqual(asset.asset_type, a_type)
        self.assertIsInstance(asset.ts_prices, TimeSeries)

    def test_importer(self):
        """Test the importer of the dataset.
        """

        from  tempfile import NamedTemporaryFile
        with NamedTemporaryFile(mode='r+', delete=False) as f:
            f.write(example_file)
            f.seek(0)
            from pybl.parser import dataset_parser
            headers, rows = dataset_parser(f.name)
            assets = Asset.populate(rows)
            self.assertEqual(len(assets), 1)
            asset = assets[0]
            self.assertEqual(asset.asset_id, 'AAPL')
            self.assertEqual(asset.asset_type, 'EQ')
            self.assertEqual(asset.market_cap, 100)
            self.assertEqual(len(asset.ts_prices), 6)
            self.assertEqual(asset.ts_prices[0], 524.99)
            self.assertEqual(asset.ts_prices[-1], 520.56)


example_file = """ASSET_ID;ASSET_TYPE;MARKET_CAP;TS_PRICES
AAPL;EQ;100;524.99,528.16,520.63,520.01,519.05,520.56
"""
if __name__ == '__main__':
    unittest.main()