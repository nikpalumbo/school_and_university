from PyQt4 import QtCore


class View(object):
    """
    This is the view in the BlackLitterman model.
    """
    def __init__(self, name, description,
                 confidence, p_array, q_array, assets):
        """
        :param name: Name of the view
        :param description: short description of the view
        :param confidence: Idzorek confidence specified as a decimal
        :param p_array: Pick matrix for the view
        :param q_array: Vector of view returns
        :param assets: List of assets
        """
        self.name = name
        self.confidence = confidence
        self.p_array = p_array
        self.q_array = q_array
        self.assets = assets
        self.description = description


class Views(QtCore.QAbstractTableModel):
    """
    This is the views model.
    """

    def __init__(self, views=[], parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._views = views
        self._headers = ["name", "confidence"]

    def rowCount(self, parent):
        return len(self._views)

    def columnCount(self, parent):
        return len(self._headers)

    def setData(self, index, view):
        self._views[index] = view
        self.dataChanged.emit(QtCore.QModelIndex(), index)
        return True

    def headerData(self, section, orientation, role):

        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return QtCore.QString(self._headers[section])
            else:
                return section

        if role == QtCore.Qt.ToolTipRole:
             return self._views[section].description

    def data(self, index, role):
        row = index.row()
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            return getattr(self._views[row],
                           self._headers[column])

    def insertRows(self, view):
        self.beginInsertRows(QtCore.QModelIndex(), 0, 1)
        self._views.insert(0, view)
        self.endInsertRows()