"""This module contains all parser for the application.
"""


def dataset_parser(filename):
    import csv
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=';',
                            quotechar='"', quoting=csv.QUOTE_ALL)
        headers = [x.lower() for x in reader.next()]
        rows = []
        for row in reader:
            a_id, a_type, a_cap, ts = row
            ts = [float(t) for t in ts.split(',')]
            rows.append(dict(zip(headers, [a_id, a_type, float(a_cap), ts])))
        return headers, rows
