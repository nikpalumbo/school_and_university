import math


class TimeSeries(object):

    def __init__(self, ts):
        self.ts = ts

    def __len__(self):
        return len(self.ts)

    def __getitem__(self, index):
        return self.ts[index]

    def size(self):
        """
        Returns the len (float) of the time series
        """
        return float(len(self.ts))

    def min(self):
        """
        Returns the minimum value.
        """
        return min(self.ts)

    def max(self):
        """
        Returns the maximum value
        """
        return max(self.ts)

    def mean(self):
        """
        Returns the mean of the time series.
        The sum is calculates with the function
        fsum to return an accurate floating
        point sum. Fsum avoids to lose precision
        by tracking multiple intermediate partial
        sum.
        """
        return math.fsum(self.ts) / self.size()

    def std(self):
        """
        Returns the standard deviation of the time series.
        """
        return math.pow(self.variance(), 0.5)

    def variance(self):
        """
        Returns the variance of the time series.
        In this case the weight are all 1 therefore
        the expteced value coincide with mean.
        """
        mean = self.mean()
        return math.fsum([math.pow(a - mean, 2) for a in self.ts]) / self.size()

    def gain(self, index):
        """
        Returns the the amount gained respect to previous value
        """
        if index == 0:
            return 0
        else:
            return (self.ts[index] - self.ts[index - 1]) / \
                  float(self.ts[index - 1])