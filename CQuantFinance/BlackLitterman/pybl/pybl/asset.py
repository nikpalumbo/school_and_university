from PyQt4 import QtCore
from pybl.core import TimeSeries

class Asset(object):
    """Define an asset object.
    """

    def __init__(self, asset_id, asset_type,
                 market_cap, ts_prices):

        self.asset_id = asset_id
        self.asset_type = asset_type
        self.market_cap = market_cap
        self.ts_prices = TimeSeries(ts_prices)

    @classmethod
    def populate(cls, rows):
        """Return a list of assets from rows.
        """
        assets = []
        for row in rows:
            assets.append(Asset(**row))
        return assets

    def __eq__(self, other):
        return (isinstance(other, self.__class__)
            and self.asset_id == other.asset_id)

    def __ne__(self, other):
        return not self.__eq__(other)


class Assets(QtCore.QAbstractTableModel):

    def __init__(self, assets=[], headers=[], pick_values=False, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._assets = assets
        self._headers = headers
        self._pick_values = pick_values

    def rowCount(self, parent):
        return len(self._assets)

    def columnCount(self, parent):
        return len(self._headers)

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return QtCore.QString(self._headers[section])
            else:
                return QtCore.QString(self._assets[section].asset_id)

    def data(self, index, role):

        row = index.row()
        column = index.column()

        value = self._assets[row]

        if role == QtCore.Qt.ToolTipRole:
            return value.asset_id

        if role == QtCore.Qt.DisplayRole:
            if hasattr(value, self._headers[column]):
                return getattr(value, self._headers[column])

    def insertRows(self, asset):
        if asset not in self._assets:
            self.beginInsertRows(QtCore.QModelIndex(), 0, 0)
            self._assets.insert(0, asset)
            self.endInsertRows()
        return True


class AssetsSelected(QtCore.QAbstractTableModel):

    def __init__(self, assets=[], parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._assets = assets
        self._headers = ['asset_id', 'value']
        self._pick_values = [0 for x in range(len(assets))]

    def rowCount(self, parent):
        return len(self._assets)

    def columnCount(self, parent):
        return len(self._headers)

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return QtCore.QString(self._headers[section])
            else:
                if len(self._assets):
                    return QtCore.QString(self._assets[section].asset_id)

    def data(self, index, role):

        row = index.row()
        column = index.column()
        value = self._assets[row]

        if role == QtCore.Qt.ToolTipRole:
            return value.asset_id

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return value.asset_id
            else:
                return self._pick_values[row]

    def insertRows(self, asset):
        if asset not in self._assets:
            self.beginInsertRows(QtCore.QModelIndex(), 0, 0)
            self._assets.insert(0, asset)
            self._pick_values.append(0)
            self.endInsertRows()
        return True

    def flags(self, index):
        col = index.column()
        if col == 1:
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled
        else:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def setData(self, index, value, role):
        col = index.column()
        row = index.row()
        if col == 1 and QtCore.Qt.EditRole:
            self._pick_values[row] = value
            return True
        return False

    def RemoveRows(self, index):
        self.beginRemoveRows(QtCore.QModelIndex(), index, 0)
        self._assets.pop(index)
        self.endRemoveRows()


class Returns(QtCore.QAbstractTableModel):

    def __init__(self, prices=[], parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._prices = prices

    def rowCount(self, parent):
        return len(self._prices)

    def columnCount(self, parent):
        return 1

    def headerData(self, section, orientation, role):

        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return QtCore.QString("Return")
            else:
                return section

    def data(self, index, role):
        row = index.row()
        if role == QtCore.Qt.DisplayRole:
            if row > 0:
                return self._prices.gain(row)
            else:
                return "-"
