import math
import numpy as np
from pprint import pprint
import scipy.stats
from pylab import *

def garch(values, n, k, alpha=0.047025272, beta=0.946624331, debug=1):
    """Returns the volatility given the time series.
    """
    n_values = len(values)
    returns = np.zeros(n_values)
    garch_vol2 = np.zeros(n_values + k)
    likelihood = np.zeros(n_values)
    annualised_vol = np.zeros(n_values + k)

    for i in range(1, n_values):
        returns[i] = math.log(values[i] / values[i - 1])

    garch_vol2[1] = np.var(returns)
    long_run = garch_vol2[1]
    omega = long_run * abs(1 - alpha - beta)

    for i in range(2, n):
        garch_vol2[i] = omega + alpha * (pow(returns[i - 1], 2)) + beta * garch_vol2[i - 1]

    # Forecast
    last_known = garch_vol2[n - 1]
    for j in range(n, n_values + k):
         garch_vol2[j] = long_run + pow((alpha + beta), j - n + 1) * (last_known - long_run)

    for i in range(1, n_values):
        my_std = math.sqrt(garch_vol2[i])
        my_norm = scipy.stats.norm(loc=0, scale=my_std)
        likelihood[i] = math.log(my_norm.pdf(returns[i]))

    for i in range(1, n_values + k):
        annualised_vol[i] = math.sqrt(garch_vol2[i] * 365) * 100


    if debug:
        print "LongRun:    {0}".format(long_run)
        print "Omega:      {0}".format(omega)
        print "Alpha:      {0}".format(alpha)
        print "Beta:       {0}".format(beta)
        print "Constraint: {0}".format(alpha + beta)
        print "Likelihood: {0}".format(sum(likelihood))
        print "Likelihoods:{0}".format(likelihood)
        print "Ann.Vol:    {0}".format(annualised_vol)

    return dict(long_run=long_run,
                alpha=alpha,
                beta=beta,
                last_known=garch_vol2[-1],
                annualised_vol=annualised_vol)


def plot_data(values1, values2):
    t = range(len(values1))
    plot(values1)
    plot(values2)

    xlabel('time (s)')
    # ylabel('voltage (mV)')
    title('About as simple as it gets, folks')
    grid(True)
    # s avefig("test.png")
    show()


if __name__ == '__main__':
    import data
    sp500 = data.sp500
    values = garch(data.itraxx_5yr_cds, len(data.itraxx_5yr_cds), k=10)
    print values['last_known']
    plot_data(values['annualised_vol'], [])
