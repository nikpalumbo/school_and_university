import sys
from pybl.resources import resources_rc
from PyQt4 import QtCore, QtGui, uic
from pybl.asset import Asset, Assets, Returns, AssetsSelected
from pybl.core import TimeSeries
from pybl.view import View, Views
from pybl.parser import dataset_parser
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg\
                                        as FigureCanvas


class mainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.ui = uic.loadUi("../resources/main.ui", self)
        self.fig = Figure((8, 2), dpi=100)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.plot_timeseries)
        self.axes = self.fig.add_subplot(111)
        self.fig_model = Figure((8, 2), dpi=100)
        self.canvas_model = FigureCanvas(self.fig_model)
        self.axes_model = self.fig_model.add_subplot(111)
        self.setupUI()
        self.assets = []
        self.returns = []
        self.views = Views([])
        self.available_views.setModel(self.views)

    def get_value_for_view(self):
        """
        Creates a dict for the view from the value of the GUI
        """
        return dict(name=self.txt_view_name.toPlainText(),
                      description=self.txt_view_desc.toPlainText(),
                      confidence=self.slider_confidence_view.value(),
                      p_array=self.slider_outperform_view.value(),
                      q_array=[],
                      assets=self.selected_assets_view)

    def setupUI(self):
        self.action_importfile.triggered.connect(self.importfile)

        self.connect(self.btn_remove_selected_assets,
                     QtCore.SIGNAL('clicked()'),
                     self.on_remove_selected_assets)

        self.connect(self.btn_save_view,
                     QtCore.SIGNAL('clicked()'),
                     self.on_save_view)

        self.connect(self.btn_add_asset_to_view,
                     QtCore.SIGNAL('clicked()'),
                     self.on_add_asset_to_view)

    def setupTableAssets(self, rows, headers):
        assets = Asset.populate(rows)
        self.assets = Assets(assets, headers)
        self.table_assets.setModel(self.assets)
        self.assetSelModel = QtGui.QItemSelectionModel(self.assets)
        self.table_assets.setSelectionModel(self.assetSelModel)
        self.table_assets.setSelectionBehavior(QtGui.QTableView.SelectRows)
        self.table_assets.hideColumn(3)
        self.table_assets.hideColumn(0)
        self.table_assets_view.setModel(self.assets)
        self.table_assets_view.setSelectionModel(self.assetSelModel)
        self.table_assets_view.setSelectionBehavior(QtGui.QTableView.SelectRows)
        self.table_assets_view.hideColumn(3)
        self.table_assets_view.hideColumn(0)

        self.selected_assets = AssetsSelected([])
        self.selected_assets_view.setModel(self.selected_assets)
        self.assetsSelectedSelModel = QtGui.QItemSelectionModel(self.assets)
        self.selected_assets_view.setSelectionModel(self.assetsSelectedSelModel)
        self.selected_assets_view.setSelectionBehavior(QtGui.QTableView.SelectRows)

        signal = """selectionChanged(QItemSelection,QItemSelection)"""
        self.connect(self.assetSelModel, QtCore.SIGNAL(signal),
                    self.update_returns)
        self.connect(self.assetSelModel, QtCore.SIGNAL(signal),
                    self.update_stats)

    def importfile(self, event, fname=''):
        if not fname:
            fname = QtGui.QFileDialog.getOpenFileName(self,
                                 'Select the file with assets time series')
        headers, rows = dataset_parser(fname)
        self.setupTableAssets(rows, headers)

    def update_returns(self, selected, deselected):
        """
        Updates the returns table, it is called by
        selectionChanged of the Asset Table.
        """
        ts = selected.indexes()[-1]
        data = ts.data().toPyObject()
        x = range(len(data))
        self.axes.hold(False)
        self.axes.plot(x, data)
        self.canvas.draw()
        self.returns = Returns(data)
        self.table_returns.setModel(self.returns)

    def update_stats(self, selected, deselected):
        """
        Updates the stats, it is called by
        selectionChanged of the Asset Table.
        """
        ts = selected.indexes()[-1]
        data = ts.data().toPyObject()
        self.lbl_volatility.setText(QtCore.QString(str(data.std())))
        self.lbl_max.setText(QtCore.QString(str(data.max())))
        self.lbl_min.setText(QtCore.QString(str(data.min())))
        self.lbl_var.setText(QtCore.QString(str(data.variance())))

    def on_save_view(self):
        """
        Add the current view to the table of available
        views.
        """
        view = View(**self.get_value_for_view())
        self.views.insertRows(view)

    def on_add_asset_to_view(self):
        """
        Add the selected asset to the current view
        """
        selected = self.table_assets_view.selectedIndexes()
        if len(selected):
            selected = selected[0]
            asset = selected.model()._assets[selected.row()]
            self.selected_assets.insertRows(asset)

    def on_remove_selected_assets(self):
        """
        Remove a selected assets from the view
        """
        self.assetsSelectedSelModel.selectedIndexes()[0]
        self.selected_assets.RemoveRows(0)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = mainWindow()
    myapp.importfile('Auto Import', fname='../data/dataset.csv')
    myapp.show()
    sys.exit(app.exec_())
