/* Esercizio del 27 / 10/ 2005
Autore : Palumbo Nicola
Matricola : 566 / 1424 ;
Data :   28 / 10 / 2005 ;
*/
#include <stdio.h>
#include <stdlib.h>


//////DICHAIRAZIONE COSTANTI//////////////
#define N 300  //Dimensione Massimo del vettore utlizzato come stack
#define MAX_STR 1000 //Dimensione massima della stringa letta
/////FINE DICHAIRAZIONE COSTANTI//////////



////DICHIARAZIONE DELLE PROCEDURE///////
int push(char item,char stack[]);
int pop(char *item,char stack[]);
//bool check_parentesi(char *str,char stack[]);
void gestione_errori(int er);
////FINE DICHIARAZIONE DELLE PROCEDURE//



////MAIN PROGRAM///////
int main(){
     char stack[N];     
     stack[0]=0;
     char str[MAX_STR];
     char var='a';
     
     
     push('b',stack);
     push('c',stack);
     push('d',stack);
     push('e',stack);
     
     int i;
     
     stack[5]='\0';
                      printf("%s\n",stack);
                       
    char *myitem;     
    pop(myitem,stack);
    pop(myitem,stack);
    pop(myitem,stack);
    pop(myitem,stack);
    pop(myitem,stack);
    pop(myitem,stack);
    i=0;
     system("PAUSE");
}



///PROCEDURA PUSH INTO STACK///////
int push(char item,char stack[]){
    int i=stack[0];
    
        if (i<N){
          stack[i+1]=item;
          stack[0]=i+1;
         }
        else 
           gestione_errori(101);
           return 1;
        
 return 0;
}   
///FINE PROCEDURA PUSH INTO STACK//





///PROCEDURA POP FROM STACK///////
int pop(char *item,char stack[]){
    int i=stack[0];
    
        if (i>0){
          *item=stack[i];
          stack[0]=i-1;
         }
        else 
           gestione_errori(102);
           return 1;
        
 return 0;
}   
///FINE PROCEDURA POP FROM STACK///







///PROCEDURA CONTROLLLA PARENTESI///////
bool check_parentesi(char *str,char stack[])


///FINE PROCEDURA CONTROLLA PARENTESI///





///PROCEDURA GESTIONE DEGLI ERRORI///////

/*
ERRORI

101) Stack Pieno
102) Stack vuoto

FINE ERRORI
*/

void gestione_errori(int er)
{
     switch (er)
     {
         case 101 :
            printf("\nERRORE : STACK PIENO\n");
         break;
         
         
         case 102 :
            printf("\nERRORE : STACK VUOTO\n");
         break;
     }
}

///FINE PROCEDURA GESTIONE DEGLI ERRORI//
