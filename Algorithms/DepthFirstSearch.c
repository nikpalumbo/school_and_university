#include<stdio.h>
#include<stdlib.h>

#define LIBERO 0
#define BLOCCATO 1
#define CONSUMATO 2
#define LBUFF 10

void dfs(int **mat,int *stato,int i,int j);
void stampa_grafo(int **grafo,int n,int m);

int main(){
	FILE *f=fopen("input.txt","r");
	int i,j=0,nodi=0,archi=0;
	char c=0;
	char buff[LBUFF];

	fscanf(f,"%d%d",&nodi,&archi);


	int **grafo=(int **)malloc(nodi*sizeof(int));
	int *stato=(int *)malloc(nodi*sizeof(int));
 
	for (i=0;i<nodi;i++)
		grafo[i]=(int *)malloc((archi+1)*sizeof(int));

	fscanf(f,"%*c",c);
	
	
	for(i=0;i<nodi;i++){
		j=0;
		do {
			 if (fscanf(f,"%[^ \n]%c", buff, &c)==0) fscanf(f,"%c", &c);
			 // else printf("\n%d: %d", atoi(buff), k);
			 else  grafo[i][j++]=atoi(buff); 
		}while (c!='\n');

	grafo[i][j]=-1;
	stato[i]=0;

	}
fclose(f);
stampa_grafo(grafo,nodi,archi);
printf("\n");

i=0;
//for(i=0;i<nodi;i++)
if (stato[i]==LIBERO)
	dfs(grafo,stato,i,0);

return 0;

}

void stampa_grafo(int **grafo,int n,int m)
{
	int i,j=0;
	for(i=0;i<n;i++){
		j=0;
		printf("\nNODO %d : ",i+1);
		while(grafo[i][j]!=-1)
			printf("%d ",grafo[i][j++]);
	}
	
}
void dfs(int **mat,int *stato,int i,int j)
{
	while(mat[i][j]!=-1){
		if (stato[mat[i][j]]==CONSUMATO)
				j++;
			
		else{
				if(stato[mat[i][j]]==BLOCCATO)
					printf("ARCO CICLICO : %d - %d",i+1,j+1);
				else{
					stato[mat[i][j]]=BLOCCATO;
					dfs(mat,stato,mat[i][j]);}
			}

		}
		stato[mat[i][j]]=CONSUMATO;

}

