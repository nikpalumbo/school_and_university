# include <stdio.h>
# define MAX 20

void insertion(int interi[MAX],int tot)
{
 int  temp;        /* Variabile temporanea per scambiare elementi */
 int  prossimo;
 int  attuale;

 for (prossimo=1;prossimo<tot;prossimo++)
	{
	 temp=interi[prossimo];
	 attuale=prossimo-1;
	 while ((attuale>=0) && (interi[attuale]>temp))
           {
	    interi[attuale+1]=interi[attuale];
	    attuale=attuale-1;
	       }
	 interi[attuale+1]=temp;
	}
}

main()
{
 int  tot;         /* Numero totale di elementi contenuti nell'array */
 int  interi[MAX]; /* Array che contiene i valori da ordinare */
 int  i,j;         /* Indici di scorrimento dell'array */

 /* Inserimento elementi nell'array  */
 printf("\nQuanti elementi deve contenere l'array (max %d elementi: ", MAX);
 scanf("%d",&tot);
 while (tot>MAX) 
    {printf("\n max %d elementi: ", MAX);
    scanf("%d",&tot);}

 for (i=0;i<tot;i++)
	{
	 printf("\nInserire il %d� elemento: ",i+1);
	 scanf("%d",&interi[i]);
	}

    insertion(interi,tot);
    /* Visualizzazione array ordinato */
    printf("\nArray Ordinato:");
    for (i=0;i<tot;i++)
       printf(" %d",interi[i]);

}

