#include <stdio.h>
#define MAX 20
         
int Pop(int S[])
{   S[0] = S[0]- 1;
    return S[S[0]+1]; }

void Push(int S[], int valore)
{ S[0] = S[0]+ 1;
  S[S[0]]= valore; }

void Stampa_Stack(int S[]) 
{
      int i;
      printf("\n Array attuale ");
      for(i=1;i<=S[0];i++) printf("\|%d",S[i]);
if (S[0]>0) printf("\|"); else printf(" STACK EMPTY "); }
          
int EmptyStack(int S[])
{ return S[0]==0; }
         
main()
{
 int S[MAX],size,i,scelta,valore;

 printf("\nQuanti elementi deve contenere lo Stack (max %d elementi): ", MAX);
 scanf("%d",&size);
 while (size>MAX) 
    {printf("\n max %d elementi: ", MAX);
    scanf("%d",&size);}

 for (i=1;i<=size;i++)
	{
	 printf("\nInserire il %d� elemento: ",i);
	 scanf("%d",&S[i]);
	}
 S[0]=size;
 do
      {
       Stampa_Stack(S);
       printf("\n scelta 1-POP, 2-PUSH, Altro Numero-USCITA : ");     
       scanf("%d",&scelta);
       switch (scelta) 
       {
             case 1: if (!EmptyStack(S)) printf("\n Top dello Stack %d", Pop(S));
                        else printf("\n spiacente, stack vuoto");
                     break;
             case 2: if (S[0]<MAX )
                     {
                        printf("\n valore da inserire nello stack: ");
                        scanf("%d",&valore);
                        Push(S,valore);
                     } 
                     else printf("\n spiacente, stack pieno");
       } /* fine switch */
      }  /* fine do */
 while(scelta==1||scelta==2);              
}    /* fine main */
