# include <stdio.h>
# define MAX 20

void quicksort(int array[MAX], int begin, int end);

int partition(int array[MAX], int begin, int end); 

main()
{

  int  interi[MAX]; /* Array che contiene i valori da ordinare */
  int  i,j;         /* Indici di scorrimento dell'array */
  int  tot;         /* Numero totale di elementi contenuti nell'array */

 /* Inserimento elementi nell'array  */
 printf("\nQuanti elementi deve contenere l'array: (max %d elementi) ", MAX);
 scanf("%d",&tot);
 while (tot>MAX) 
    {printf("\n max %d elementi: ", MAX);
    scanf("%d",&tot);}

 for (i=0;i<tot;i++)
	{
	 printf("\nInserire il %d� elemento: ",i+1);
	 scanf("%d",&interi[i]);
	}

    quicksort(interi, 0, tot-1);
    /* Visualizzazione array ordinato */
    printf("\nArray Ordinato:");
    for (i=0;i<tot;i++)
       printf(" %d",interi[i]);
}

void quicksort(int array[MAX], int begin, int end) 
{
   int q;
   if (begin<end) 
      { q=partition(array, begin, end);
        quicksort(array, begin, q);     
        quicksort(array, q+1, end);
      }     
}

int partition(int array[], int begin, int end) 
{   
      int pivot,l,r,tmp;
      pivot = array[begin];
      l = begin - 1;
      r = end+1;
      while(1) 
      {
       do r=r-1; 
       while(array[r]>pivot);
       do l=l+1; 
       while(array[l]<pivot);
       if (l<r) {
          tmp=array[l];
          array[l]=array[r];
          array[r]=tmp;}
       else return r;
      }
}
