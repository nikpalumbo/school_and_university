# -*- coding: utf-8 -*-

from filemanager import FileManager
from infomanager import InfoManager
import sys
fm = FileManager()
im = InfoManager()

if "addfile" in sys.argv[1]:
    artist = sys.argv[2].split('=')[1]
    work = sys.argv[3].split('=')[1]
    file = sys.argv[1].split('=')[1]
    fm.addFile(file, artist, work)

if "createartist" in sys.argv[1]:
    artist = sys.argv[1].split('=')[1]
    rs = im.createArtist(artist)
    print "*"*78
    if rs == 1:
        print "Artista %s aggiunto con successo" % artist
    else:
        print "Artista %s già presente" % artist
    print "*"*78
    

if "creatework" in sys.argv[1]:
    artist = sys.argv[1].split('=')[1]
    work = sys.argv[2].split('=')[1]
    fm.createWorkDirectory(artist, work)

if "getfiles" in sys.argv[1]:
    artist = sys.argv[1].split('=')[1]
    work = sys.argv[2].split('=')[1]
    files = fm.getFiles(artist, work)
    print "*"*78
    print files
    print "*"*78

if "removeartist" in sys.argv[1]:
    artist = sys.argv[1].split('=')[1]
    fm.deleteArtistDirectory(artist)

if "removefile" in sys.argv[1]:
    artist = sys.argv[2].split('=')[1]
    work = sys.argv[3].split('=')[1]
    file = sys.argv[1].split('=')[1]
    fm.removeFile(file, artist, work)

if "readfile" in sys.argv[1]:
    artist = sys.argv[2].split('=')[1]
    work = sys.argv[3].split('=')[1]
    file = sys.argv[1].split('=')[1]
    print fm.getFileContent(file, artist, work)

if "removework" in sys.argv[1]:
    artist = sys.argv[1].split('=')[1]
    work = sys.argv[2].split('=')[1]
    fm.deleteWorkDirectory(artist, work)
    
"""
artists = fm.getArtistsDirectories()
print "*"*78
for artist in artists:
    print "ARTISTA --> ", artist
    print "-"*78
    for work in fm.getWorksDirectories(artist[0]):
        print work
"""
