from config import SHARED_DIRECTORY
import os
import shutil

class FileManager:
    """
        Questa classe implementa la gestione a 3 livelli del filesystem
    """
 
    def addFile(self, file, artist, work, force=True):
        path = os.path.join(SHARED_DIRECTORY, artist, work)
        try:
            shutil.copy2(file, path)
            return 1
        except IOError, e:
            if force and not os.path.exists(path):
                self.createArtistDirectory(artist)
                self.createWorkDirectory(artist, work)
                self.addFile(file, artist, work)
            else:
                return e

    def getArtistsDirectories(self):
        return [name for name in os.walk(SHARED_DIRECTORY) if "artist.xml" in name[2]]

    def getArtistInfo(self, artist, work, filename):
        path = os.path.join(SHARED_DIRECTORY, artist, "artist.xml")
        return self.getFileContent(path)
        
    def getFileContent(self, path):
        fd = open(path)
        content = fd.read()
        fd.close()
        return content
        
    def getFiles(self, artist, work):
        workdirectory = os.path.join(SHARED_DIRECTORY, artist, work)
        return [filename for filename in [files for (root, dirs, files) in os.walk(workdirectory)][0] if filename != "details.xml"]
        
    def getWorksDirectories(self, artistpath):
        return [name for name in os.walk(artistpath) if "details.xml" in name[2]]
    
    def createArtistDirectory(self, artist):
        artistdirectory = os.path.join(SHARED_DIRECTORY, artist)
        try:
            os.mkdir(artistdirectory)
            fd = open(os.path.join(artistdirectory, "artist.xml"), "w")
            fd.close()
            return 1
        except OSError:
            if os.path.exists(os.path.join(SHARED_DIRECTORY, artist)):
                return 0 

    def deleteArtistDirectory(self, artist):
	shutil.rmtree(os.path.join(SHARED_DIRECTORY, artist))

    def deleteWorkDirectory(self, artist, work):
        shutil.rmtree(os.path.join(SHARED_DIRECTORY, artist, work))
    
    def createWorkDirectory(self, artist, work, force=True):
        workdirectory = os.path.join(SHARED_DIRECTORY, artist, work)
        try:
            os.mkdir(workdirectory)
            return 1
        except OSError:
            #caso in cui non esiste l'artista 'artist': si crea prima la directory
            #dell'artista, poi quella del lavoro
            if force and not os.path.exists(os.path.join(SHARED_DIRECTORY, artist)):
                self.createArtistDirectory(artist)
                self.createWorkDirectory(artist, work)
            else:
                return e
        fd = open(os.path.join(workdirectory, "details.xml"), "w")
        fd.close()

    def removeFile(self, filename, artist, work, force=True):
        #elimina un file all'interno di un lavoro
        path = os.path.join(SHARED_DIRECTORY, artist, work, filename)
	os.remove(path)

    def setArtistInfo(self, artist, info):
        #scrive il documento XML 'info' nel file cercato
        fd = open(os.path.join(SHARED_DIRECTORY, artist, "artist.xml"), "w")
        print "*"*78
        print info
        print "*"*78
        fd.close()
