from filemanager import FileManager
from xml.dom import minidom

class InfoManager:
    """
        Gestisce le informazioni associate agli artisti e ai lavori
    """
    fm = FileManager()

    def createArtist(self, artist):
        """
            Crea la directory
            crea in artist.xml il tag <artist name="ARTIST">.....</artist>
        """
        rs = self.fm.createArtistDirectory(artist)
        if rs == 1:
            doc = minidom.Document()
            artist_element = doc.createElementNS("http://www.lab32.org/musync/artist", "artist")
            doc.appendChild(artist_element)
            artistname = doc.createTextNode(artist)
            artist_element.appendChild(artistname)
            self.fm.setArtistInfo(artist, artist_element.toxml())
            return 1
        return 0

    def setArtistBirthDate(self, artist, birthdate):
        """
            Imposta in artist.xml il tag <birthdate>XXXX</birthdate>
        """
        pass

    def setBiography(self, artist, text):
        """
            Imposta in artist.xml il tag <biography>TEXT</biography>
        """
        pass

    def addMember(self, artist, member, startdate, enddate):
        """
            Imposta in artist.xml il tag <member startdate="startdate" enddate="enddate">MEMBER</member>
        """
        pass

    def removeMember(self, artist, member):
        """
            Elimina dalle informazioni member
        """
        pass

        
    def setName(self, artist, birthdate):
        """
            Imposta in artist.xml il tag <artist>XXXX</artist>
        """
        pass
