from musync.config import SHARED_DIRECTORY, PORT, ELECTION
import os
import shutil
import pickle
import Pyro
import time
class Operation(object):
    """
        tipo append usato come parametro per il metodo manageObject della classe FileServer
        kwargs imposta tutte le variabili usate da op
        op deve essere una funzione (anche lambda) ad un parametro ed usa le variabili di kwargs
    """
    op = ""
	
    def __init__(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])

class Artista(object):
    """
    Classe rappresentante un artista
    """
    def __init__(self):
        self.nome = ""
        self.hostid = 000000
        self.crev = 0
        self.mrev = 0
        self.hosts = dict()
        self.albums = []

class Album(object):
    """
        Classe rappresentante un album
    """

    def __init__(self):
        self.nome = ""
        self.artista = ""
        self.brani = []

class Brano(object):
    """
        Classe rappresentante un brano
    """
    def __init__(self):
        self.nome = ""
        self.artista = ""
        self.album = ""

class FileServer:
    """
        Questa classe implementa la gestione a 3 livelli del filesystem
    """
	
    def __init__(self, shared_directory=''):
        #se non esiste la cartella condivisa allora la crea
        self.sd = shared_directory
        if self.sd == '':
            self.sd = SHARED_DIRECTORY
        if not os.path.exists(self.sd):
            os.mkdir(self.sd)			
 
    def addFile(self, file, artist, album, force=True):

        if len(self.getArtist(artist)) == 0:
            self.createArtist(artist)
        if len(self.getAlbum(artist, album)) == 0:
            self.createAlbum(artist, album)

        path = os.path.join(self.sd, artist, album)
    
        #aggiunge il file al percorso calcolato
        rs =shutil.copy2(file, path)

        #crea l'oggetto Brano e lo pickla nella directory
        filename = os.path.split(file)[-1]
        b = Brano()
        path = os.path.join(path, filename + ".obj")
        self.manageObject("create", path, b, nome=filename, artista=artist, album=album)

        #aggiorna l'artista che contiene l'album
        path = os.path.join(self.sd, artist, "artist.obj")
        incr = Operation(k=None)
        incr.op = lambda k : k+1
        self.manageObject("modify", path, None, crev=incr, mrev=incr)

        #aggiorna l'album che contiene il brano
        path = os.path.join(os.path.join(self.sd, artist, album), "album.obj")
        append = Operation(filename=filename)
        append.op = lambda k : k + [append.filename]
        self.manageObject("modify", path, None, brani=append)
        return 1

    def checkAlbumsForArtist(self, artistpath):
        #fa in modo che l'attributo albums in artist.obj corrisponda con gli album effettivamente presenti
        path = os.path.join(artistpath, "artist.obj")
        artistobj = self.manageObject("load", path, None, x=0)
        actualalbums = [i for i in os.listdir(artistpath) if i != "artist.obj"]

        setalbums = Operation(actualalbums=actualalbums)
        setalbums.op = lambda k : setalbums.actualalbums
        self.manageObject("modify", path, None, albums=setalbums)
        artistobj = self.manageObject("load", path, None, x=0)


    def compareNames(self, a, b, equal=False):
        """
            Effettua una comparazione key-insensitive
            Se equal e' True la comparazione e' di tipo *a*
        """
        if equal:
            if a.lower() == b.lower():
                return True 
        else:
            if a.lower() in b.lower():
                return True
        return False

    def createArtist(self, artist):
        artistdirectory = os.path.join(self.sd, artist)
        if len(self.getArtist(artist)) == 0:
            #crea la directory dell'artista
            os.mkdir(artistdirectory)
			
            #crea l'oggetto Artista e lo pickla nella directory
            a = Artista()
            path = os.path.join(artistdirectory, "artist.obj")

            obj = Pyro.core.getProxyForURI("PYROLOC://localhost:%s/fileserver" % PORT)
            starttime = time.time()
            while 1:
                myid = obj.getID()
                if myid != ELECTION:
                    break
                print "waiting for hostlist"
                time.sleep(2)
            else:
                myid = 0
            self.manageObject("create", path, a, nome=artist, crev=1, mrev=1, hostid = myid)
            return 1
        else:
            print "Artista <%s> gia' presente" % (artist)
            return 0			

    def createAlbum(self, artist, album, force=True):
        albumdirectory = os.path.join(self.sd, artist, album)
        if len(self.getArtist(artist)) == 0:
            self.createArtist(artist)
        if len(self.getAlbum(artist, album)) == 0:
            #crea la directory dell'album
            os.mkdir(albumdirectory)
            #crea l'oggetto Album e lo pickla nella directory
            a = Album()
            path = os.path.join(albumdirectory, "album.obj")
            self.manageObject("create", path, a, nome=album, artista=artist)
            
            #aggiorna l'artista che contiene l'album
            path = os.path.join(os.path.join(self.sd, artist), "artist.obj")
            incr = Operation(k=None)
            incr.op = lambda k : k+1
            append = Operation(album=album)
            append.op = lambda k : k + [append.album]
            self.manageObject("modify", path, 0, crev=incr, mrev=incr, albums=append)
            return 1
        else:
            print "Album <%s> gia' presente" % (album)
            return 0


    def loadAlbum(self, artist, albumpath, force=True):
        """
            Carica un album intero (presente in "albumpath") nell'artista "artist"
        """
        album = os.path.split(albumpath)[-1]
        if len(self.getAlbum(artist, album)) == 0:
            #albumdirectory = os.path.join(SHARED_DIRECTORY, artist, albumname)
            genobj = os.walk(albumpath)
            for files in genobj:
                #controlla se l'album sorgente e' pieno
                if len(files[2]):
                    for file in files[2]:
                        #richiama il metodo addFile per aggiungere un file dall'album sorgente
                        if ".db" not in str(file) and ".jpg" not in str(file):
                            self.addFile(files[0] + os.sep + file, artist, album)
            return 1
        else:
            print "Album <%s> gia' presente" % (album)
            return 0            		

    def getAlbum(self, artist=None, album=None, equal=False):
        """
            Ritorna l'album "album" dell'artista "artist"
            Se artist non e' specificato, ritorna tutti gli album "album"
            Se album non e' specificato, ritorna tutti gli album
            Il formato di ritorno e' una lista di coppie, ogni coppia e' nel formato (albumpath, album.obj)
        """
        albums = []

        artists = self.getArtist(artist, equal)
        #itera sulle cartelle contenute nella shared directory
        for art in artists:
            #for alb in art[1].getAlbums():
            for alb in art[1].albums:
                if album == None or self.compareNames(album, alb, equal):
                    albumdir = os.path.join(art[1].nome, alb)
                    path = os.path.join(self.sd, albumdir, "album.obj")
                    albumobj = self.manageObject("load", path, None, x=0)
                    albums.append((albumdir, albumobj))
        return albums

    def getArtist(self, artist=None, equal=False):
        """
            Ritorna tutti gli artisti in base ad una ricerca LIKE-type sulla stringa "artist"
            Se "artist" non e' specificato, ritorna tutti gli artisti
            Il formato di ritorno e' una lista di coppie, cui ciascuna e' composta da <nome cartella>,istanza di Artista
        """
        artists = os.walk(self.sd).next()[1]
        artistslist = []
        for art in artists:
            if artist == None or self.compareNames(artist, art, equal):
                artistobj = self.manageObject("load", os.path.join(self.sd, art, "artist.obj"), None, x=0)
                artistslist.append((art, artistobj))
        return artistslist
		
    def getFile(self, artist=None, album=None, filename=None, equal=False):
        """
            Cerca il file con nome 'filename' per artista album o entrambi
            Se non e' specificato nulla ritorna tutto cio' che e' condiviso
		"""
        files = []
        artists = self.getArtist(artist, equal)
        for art in artists:
            albums = self.getAlbum(art[1].nome, album, equal)
            for alb in albums:
                for brano in alb[1].brani:
                    if filename == None or self.compareNames(filename, brano, equal):
                        path = os.path.join(self.sd, art[1].nome, alb[1].nome, brano + ".obj")
                        fileobj = self.manageObject("load", path, None, x=0)
                        files.append((os.path.join(fileobj.artista, fileobj.album, fileobj.nome), fileobj))
        return files

    def getFileContent(self, path):
        """
            ritorna il contenuto di un file
        """
        fd = open(path)
        content = fd.read()
        fd.close()
        return content

    def manageObject(self, actype, filename, instance, **kwargs):
        if actype == "load":
            fd = open(filename, "r")
            return pickle.load(fd)
        elif actype == "modify":
            instance = self.manageObject("load", filename, 0, x=0)
        elif actype!="create":
            raise("I valori possibili per il parametro actype sono 'load', 'modify' e 'create'")

        #N.B. questo blocco viene eseguito solo se actype="create" oppure actype="modify"
        for key in kwargs:
            if kwargs[key].__class__.__name__ == "Operation":
                value = kwargs[key].op(getattr(instance, key))
            else:
                value = kwargs[key]
            setattr(instance, key, value)
        fd = open(filename, "w+")
        pickle.dump(instance, fd)
        fd.close()
        return instance

    def renameFile(self, fileobj, newname):
        """
            rinomina il file relativo a fileobj
        """
        #FIXME Completare!!!
        splitted_name = fileobj.nome.split(".")
        effective_name = ".".join(splitted_name[:-1])
        effective_suffix = splitted_name[-1]
        newname = newname + "." + effective_suffix
        #rinomina fileobj
        objold = os.path.join(self.sd, fileobj.artista, fileobj.album, fileobj.nome + ".obj")
        objnew = os.path.join(self.sd, fileobj.artista, fileobj.album, newname + ".obj")
        os.rename(objold, objnew)
        #rinomina il file musicale
        musold = os.path.join(self.sd, fileobj.artista, fileobj.album, fileobj.nome)
        musnew = os.path.join(self.sd, fileobj.artista, fileobj.album, newname)
        os.rename(musold, musnew)
        #rinomina il nome del file nell'album
        #completare QUI!!!
        fd = open(os.path.join(self.sd, fileobj.artista, fileobj.album), "rw")
        print "-"*78
        #aggiorna crev ed mrev
        print os.path.join(self.sd, fileobj.artista)
        print "-"*78

        
    def deleteArtistDirectory(self, artist):
	    shutil.rmtree(os.path.join(self.sd, artist))

    def deletealbumDirectory(self, artist, album):
        shutil.rmtree(os.path.join(self.sd, artist, album))

    def removeFile(self, filename, artist, album, force=True):
        #elimina un file all'interno di un lavoro
        path = os.path.join(self.sd, artist, album, filename)
        os.remove(path)
