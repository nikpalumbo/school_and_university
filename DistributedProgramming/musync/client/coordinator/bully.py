import threading
from musync.config import  JOINED, DISJOINT, LEADER, ELECTION, WAITINGACCESS, TRANSITIONS, JOINTOGROUP, WORK,STARTELECTION, ACCESSGROUPBYMS, WHOISBULLY, NOTIFYALL, WAITINGBULLY, MAXWAITINGNOTIFY
import Pyro.core
from Pyro.core import PyroURI
import random
import time
class Coordinator(threading.Thread):
    def __init__(self, ms, myuri, logger):
        super(Coordinator,self).__init__()
        self.ms = ms
        self.myuri = myuri
        self.myid = 0
        self.logger = logger
        self.bs = ""
        self.state = DISJOINT
        self.transition = JOINTOGROUP
        self.hostlist = {}
        self.elapsedtime = 0

    def accessGroupByMS(self):
        hostlist, self.myid = self._request("accessRequest", self.ms, self.myuri)
        self.logger.info("Access to Community By Musync Network Server")
        if type(hostlist) == dict:
            self.hostlist = hostlist
            self.logger.info("Host list is :%s" % str(hostlist))
            res = self._request("updateHostList", self.ms, self.hostlist)
            self.logger.info("Updated Hostlist on Musync Server Network, server's response is: %s" % res)
            self.transition = STARTELECTION
            self.state = JOINED
    
    def run(self):
        while 1:
            time.sleep(5)
            self.doTransition(self.state, self.transition)

    def doTransition(self, state, transition):
        self.logger.debug("Called doTransition")
        if transition in TRANSITIONS[state]:
            getattr(self, transition)()
        else:
            self.logger.info("Cannot change state: %s --> %s" % (state, transition))
    
    def joinToGroup(self):
        self.bs = self._request("getCurrentBS", self.ms)
        if self.bs == self.myuri:
            self.transition = WHOISBULLY
            self.state = LEADER
            self.hostlist = self._request("getHostList", self.ms)
            self.myid = [ x for x in self.hostlist.keys() if self.hostlist[x] == self.myuri][0]
            self.logger.info("My Last ID was %s" % self.myid)
            return
        if self.bs:
            if self.bs == "NOBS":
                self.transition = ACCESSGROUPBYMS
                self.logger.info("Musync Community no BS")
            else:
                res = self._request("getHostList", self.bs, (self.myid, self.myuri))
                if res is None:
                    self.transition = JOINTOGROUP
                    self.logger.info("Request host list  to Musync Server Coordinator at %s" % self.bs)
                else:
                    #IO CLIENT AVVISO ANCHE IL MS ...IN REALTA' DOVREI AVVISARE IL BS CHE A SUA VOLTA AVVISA IL MS
                    self.hostlist, self.myid = res[0], res[1]
                    res = self._request("updateHostList", self.ms, self.hostlist)
                    self.logger.info("Updated Hostlist on Musync Server Network, server's response is: %s" % res)
                    if len(self.hostlist):
                        self.logger.info("JOINED to Musync Community number host --> %d" % (len(self.hostlist)))
                        for host in self.hostlist.keys():
                            res = self._request("updateHostList", self.hostlist[host], self.hostlist)
                            self.logger.info("Updated Hostlist Client ID %d response is: %s" % (host, res))
                        if self.bs == self.myuri:
                            self.transition = WHOISBULLY
                            self.state = LEADER
                        else:
                            res = self._request("getID", self.bs)
                            if res < self.myid:
                                self.transition = STARTELECTION
                                self.state = ELECTION
                            else:
                                self.transition = WORK
                                self.state = JOINED
                    #else:
                    #    self.transition = JOINED
                    #    self.logger.info("Musync Community STARTELECTION number host --> %d" % (len(self.hostlist) + 1))
        else:
            self.logger.info("Cannot contact Musync Server for joining Musync Community")
            self.transition = JOINTOGROUP

    def notifyAll(self):
        self.logger.debug("NotifyAll")
        self._request("setCurrentBS",self.ms, self.myuri)
        self.logger.info("Musync Server know who is the new Coordinator: %s" % self.myuri)
        for host in self.hostlist.keys():
            self._request("setCurrentBS", self.hostlist[host], self.myuri)
            self.logger.debug("%s know who is the new Coordinator: %s" % (self.hostlist[host], self.myuri))
        self.state = LEADER
        self.transition = WHOISBULLY
 
    def startElection(self):
        self.logger.debug("Called startElection")
        self.state = ELECTION
        if len(self.hostlist) == 1:
            if self._request("setCurrentBS",self.ms, self.myuri):
                self.state = LEADER
                self.transition = WHOISBULLY
        else:
            #AVVIARE LA VERA ELEZIONE
            self.logger.debug("Starting alert all client")
            leader = [hostid for hostid in self.hostlist.keys() if hostid > self.myid]
            self.logger.info("Leader possible are %s" % str(leader))
            for host in leader:
                self.logger.debug("HOST CONTACTING %d uri is %s" % (host, self.hostlist[host]))
                if self._request("electMe", self.hostlist[host], self.myid) == "NO":
                    self.logger.info("ElectMe for id %d host Response No")
                    self.transition = WAITINGBULLY
                    break
                else:
                    self.logger.info("ElectMe for id %d Connection Failed" % host)
            else:
                self.transition = NOTIFYALL

    def waitingBully(self):
        self.logger.info("Waiting Bull")
        self.elapsedtime += 1
        if self.elapsedtime > MAXWAITINGNOTIFY:
            self.transition = STARTELECTION

    def whoIsBully(self):
        self.logger.debug("Called whoIsBully")
        if self._request("amIBully",self.ms, self.myuri):
            self.logger.info("My ID:%d I'm spaccon" % self.myid)
        else:
            self.state = JOINED
            self.transition = STARTELECTION

    def work(self):
        self.logger.debug("Called work")
        if self._request("isBullyUp",self.bs):
            self.logger.info("My ID:%d Bully is %s" % (self.myid,self.bs))
        else:
            self.transition = STARTELECTION

    def _request(self, method, uri, param=None):
        try:
            Pyro.core.initClient()
            proxy=Pyro.core.getProxyForURI(uri)
            if param:
                res = getattr(proxy, method)(param)
            else:
                res = getattr(proxy, method)()
            self.logger.debug("Method invocation URI=%s METHOD=%s RESULT=%s" % (uri, method, str(res)))
            return res
        except Exception, e:
            self.logger.debug("Error method invocation URI=%s METHOD=%s" % (uri, method))
            self.logger.debug(e)
            return None

class MusyncElectionLeader:

    def __init__(self, ms, myuri, logger):
        self.logger = logger
        self.coordinator = Coordinator(ms, myuri, logger)
    

    def electMe(self, idhost=0):
        self.logger.info("Called Method electMe --> id=%s" % str(idhost))
        if self.coordinator.myid > idhost:
            self.logger.info("Client id %d asked for election" % idhost)
            if not self.coordinator.state == ELECTION and not self.coordinator.state == LEADER:
                self.coordinator.transition = "startElection" 
            return "NO"
        else:
            #FIXME PROBLEMA DA RISOLVERE SI PALLEGGIA!!!!
            self.transition = WAITINGBULLY
            return True

    def isBullyUp(self):
        return True

    def getHostList(self, iduri):
        self.logger.debug("Called Method getHostlist")
        self.logger.info(iduri)
        idhost, uri = iduri[0], iduri[1]
        if uri in self.coordinator.hostlist.values():
            idhost = [x for x in self.coordinator.hostlist.keys() if uri == self.coordinator.hostlist[x]][0]
        if idhost == 0:
            idhost =  self._getUniqueID()
        if len(iduri) and not self.coordinator.hostlist.has_key(idhost):
            self.logger.info("Client joined to community: id %s uri %s" %  iduri)
            self.coordinator.hostlist[idhost] = uri
        return self.coordinator.hostlist, idhost

    def getHostListForClient(self):
        
        if self.coordinator.state == ELECTION:
            return ELECTION
        else:
            return self.coordinator.hostlist

    def getID(self):

        if self.coordinator.state == ELECTION:
            return ELECTION
        else:
            return self.coordinator.myid 

    def setCurrentBS(self, bs):
        self.logger.debug("Called Method setCurrentBS")
        self.coordinator.bs = bs
        self.coordinator.transition = WORK
        self.coordinator.state = JOINED
        self.logger.info("BS Settato ----------------> %s" % bs)
        return True
        
    def start(self):
        self.logger.debug("Called Method start")
        self.coordinator.start()
   
    def updateHostList(self, hostlist):
        self.logger.info("New host list received: %s " % str(hostlist))
        self.hostlist = hostlist
        return "OK"
    
    def _getUniqueID(self):
        while 1:
            tmp = int(random.random() * 100)
            if tmp not in self.coordinator.hostlist.keys() and tmp > 0:
                self.coordinator.logger.debug("New id in Community %d" % tmp)
                return tmp
            if self.coordinator.hostlist.keys() >= 99:
                raise Exception("Cannot obtaine unique ID")
