import os, sys, time, threading
import Pyro.core
from musync.config import CHUNK_SIZE, SHARED_DIRECTORY, ELECTION
from musync.client.coordinator import MusyncElectionLeader
from musync.client.fs import FileServer
import logging
from Pyro.core import PyroURI
import time
import sys
class PyroFileServer(Pyro.core.ObjBase, FileServer, MusyncElectionLeader):

    def __init__(self, path, ipmusyncserver, portmusyncserver, logger):
        Pyro.core.ObjBase.__init__(self)
        FileServer.__init__(self, path)
        MusyncElectionLeader.__init__(self, ipmusyncserver, portmusyncserver, logger)
        self.path = path

    def openFile(self, fd, dtype, tarfilename):
        if hasattr(self.getLocalStorage(), "openfile"):
            raise IOError("Puo' essere letto un solo file per volta")
        artist = os.path.join(self.path.replace(" ", "\ "), fd.split(os.sep)[0])
        #FIXME invece di tmp.tar il file deve avere il nome del file che si sta scaricando
        self.tmppath = os.path.join(self.path.replace(" ", "\ "), tarfilename)
        if dtype != "artista":
            #si vuole effettuare il download di un solo album
            #per cui si scarica artista, album e artist.obj
            artistobj = os.path.join(fd.replace(" ", "\ ").split(os.sep)[0], "artist.obj")
            os.system("tar -C %s -cf %s %s -i %s" % (self.path.replace(" ", "\ "), self.tmppath, fd.replace(" ", "\ "), artistobj))
        else:
            #si effettua il download dell'artista intero
            os.system("tar -C %s -cf %s %s" % (self.path.replace(" ", "\ "), self.tmppath, fd.replace(" ", "\ ")))
        self.getLocalStorage().openfile=open(self.tmppath, "rb")
        return os.path.getsize(self.path)

    def retrieveNextChunk(self):
        chunk = self.getLocalStorage().openfile.read(CHUNK_SIZE)
        if chunk:
            return chunk
        self.getLocalStorage().openfile.close()
        return ''

    def closeFile(self):
        self.getLocalStorage().openfile.close()
        del self.getLocalStorage().openfile
        #os.system("rm " + self.tmppath)

def musyncDaemon(argv):

    logger = logging.getLogger("Musync")
    logger.setLevel(logging.INFO)
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)
    
    Pyro.core.initServer()
    daemon = Pyro.core.Daemon()
    obase = PyroFileServer(SHARED_DIRECTORY, PyroURI(argv[0], 'MusyncNetwork', int(argv[1]), "PYROLOC"), PyroURI(daemon.hostname, 'fileserver', daemon.port, "PYROLOC"), logger)
    obase.start()
    daemon.connect(obase, 'fileserver')
    logger.info("Musync daemon started at %s:%s port" % (daemon.hostname,daemon.port))

    daemon.requestLoop()

if __name__ == "__main__":
    if len(sys.argv) == 3:
        try:
            musyncDaemon(sys.argv[1:])
        except KeyboardInterrupt:
            print "Wait Please..."
            time.sleep(6)
            print "Bye Bye"
            sys.exit()
            
    else:
        print 
        print "USAGE: IP ADDRESS SERVER MUSYNC - PORT SERVER MUSYNC"
        print 
