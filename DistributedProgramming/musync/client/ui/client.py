import Pyro.core
import sys, time, random, threading, os
from exceptions import ValueError
from musync.config import SHARED_DIRECTORY
from musync.client.fs import FileServer
from musync.config import DTYPE, PORT, ELECTION
import os

#buffer che serve per passare il messaggio tra un produttore e un consumatore
BUFFER = [{}]

#lista usata dal consumatore per memorizzare i dati passati col buffer
LST=[]

#evento (segnale) usato da un produttore per comunicare che il risultato e' prodotto e salvato nel buffer
SEARCHED = threading.Event()

#lock per la mutua esclusione sul buffer
LOCK = threading.Lock()

def showMenu(menu):
    os.system(['clear','cls'][os.name == 'nt'])
    if menu == 1:
        print "_"*30
        print "-\/-MUSYNC-/\-"
        print "-"*30
        print "1) Gestione File"
        print "2) Download| Sincronizzazione"
        print "Q) Uscita"
    if menu == 2:
        print "1) Ricerca/download artista"
        print "2) Ricerca/download album"
        print "3) Ricerca/download brano"
        print "Q) Torna al menu principale"
    if menu == 3:
        print "1) Aggiungi artista"
        print "2) Ricerca propri artisti"
        print "3) Aggiungi album"
        print "4) Ricerca propri album"
        print "5) Carica album"
        print "6) Aggiungi brano"
        print "7) Ricerca propri brani"
        print "Q) Torna al menu principale"

def showAddResults(res):
    print "_"*20
    if res == 1:
        print "file aggiunto con successo"
    else:
        print "file non aggiunto"
    print "_"*20

def showGetArtistResults(res):
    print "_"*20
    if len(res) > 0:
        for r in res:
            print "Nome artista:", r[0]
            print "versione:", r[1].mrev
            print "album presenti:"
            if len(r[1].albums) > 0:
                for album in r[1].albums:
                    print "\t", album
            else:
                print "\tnessun album presente"
            print "_"*20
    else:
        print "Nessun artista corrispondente alla ricerca"
        print "_"*20

def showGetAlbumResults(res):
    print "_"*20
    if len(res) > 0:
        for r in res:
            print "Nome artista:", r[1].artista
            print "Nome album:", r[1].nome
            print "brani presenti:"
            if len(r[1].brani) > 0:
                for brano in r[1].brani:
                    print "\t", brano
            else:
                print "\tnessun brano presente"
    else:
        print "Nessun album corrispondente alla ricerca"
        print "_"*20

def showGetFileResults(res):
    print "_"*20
    if len(res) > 0:
        for r in res:
            print "artista:", r[1].artista
            print "album:", r[1].album
            print "nome:", r[1].nome
            print "_"*20
    else:
        print "Nessun brano corrispondente alla ricerca"        
        print "_"*20

def dummy(res):
    print res
    print "FIXME implementare!!!"


FILEMANAGER_ACTIONS = {'1': [
                                ["artist", "ARTISTA_"], 
                                FileServer().createArtist,
                                showAddResults
                            ],
                       '2': [
                                ["artist", "ARTISTA_"],
                                FileServer().getArtist,
                                showGetArtistResults,
                            ], 
                       '3': [
                                ["artist", "ARTISTA_"], 
                                ["album", "ALBUM_"], 
                                FileServer().createAlbum,
                                showAddResults
                            ], 
                       '4': [
                                ["artist", "ARTISTA_"], 
                                ["album", "ALBUM_"], 
                                FileServer().getAlbum,
                                showGetAlbumResults
                            ], 
                       '5': [
                                ["artist", "ARTISTA_"], 
                                ["albumpath", "PERCORSO ALBUM_"], 
                                FileServer().loadAlbum,
                                showAddResults
                            ], 
                       '6': [
                                ["file", "PERCORSO FILE_"], 
                                ["artist", "ARTISTA_"], 
                                ["album", "ALBUM_"], 
                                FileServer().addFile,
                                showAddResults
                            ], 
                       '7': [
                                ["artist", "ARTISTA_"], 
                                ["album", "ALBUM_"], 
                                ["filename", "BRANO_"], 
                                FileServer().getFile,
                                showGetFileResults
                            ],
                      }

#thread di ricerca su di un singolo server
class doSearch(threading.Thread):

    def __init__(self, host, BUFFER, searchtype="", **kwargs):
        threading.Thread.__init__(self)
        #identificativo del produttore (coppia indirizzo e porta)
        self.host = host

        #in questo modo il produttore sa qual e' il buffer
        self.buffer = BUFFER

        #tipo di ricerca, searchtype puo' avere come valore 'getArtist', 'getAlbum', 'getFile'
        self.searchtype = searchtype
        self.kwargs = kwargs

    def run(self):
        #acquisisce il mutex (sara' rilasciato dal consumatore dopo che ha messo da parte il risultato)
        LOCK.acquire()

        #FIXME l'attesa e' solo a titolo di esempio, poi si cancellera', serve per simulare un tempo di attesa
        #il produttore i attende x secondi, dove x e' l'ultima cifra della porta
        try:
            obj = Pyro.core.getProxyForURI(self.host)
            self.buffer[0] = {'address' : self.host.address,
                         'port' : self.host.port,
                         'result' :getattr(obj, self.searchtype)(**self.kwargs)
                             }
        except Exception, e:
            print "ERRORE", e
            self.killed = True

        #invia un segnale al consumatore
        SEARCHED.set()

        #il thread si ammazza
        self.killed = True


#thread showSearch per mostrare i risultati della ricerca distribuita
class showSearch(threading.Thread):

    def __init__(self, nhosts):
        threading.Thread.__init__(self)
        self.nhosts = nhosts

    def run(self):
        #try:
            while len(LST) > 0:
                LST.pop()
            for i in range(self.nhosts):

                #attende l'invio del segnale da parte di un produttore
                SEARCHED.wait()
                #stampa il contenuto del buffer e lo salva
                for host in [host for host in BUFFER if host != {}]:
                    for rs in host['result']:
                        LST.append(host)
                        print str(len(LST)-1) + ")"
                        print rs[0]
                        print "Provenienza:", host['address']+":"+str(host['port'])
                        artistobj = rs[1]
                        if rs[1].__class__.__name__ == "Album" or rs[1].__class__.__name__ == "Brano":
                            obj = Pyro.core.getProxyForURI("PYROLOC://%s:%s/fileserver" % (host['address'], host['port']))
                            artistobj = obj.getArtist(rs[1].artista, equal=True)[0][1]
                        print "Creatore:", artistobj.hostid
                        print "Versione corrente:", artistobj.crev, "\tVersione massima:", artistobj.mrev
                        print "_"*78
                        #print str(len(LST)) + ") ", host['address']+":"+str(host['port']), " ---> ", rs[0]
                #annulla il segnale
                SEARCHED.clear()
    
                #rilascia il mutex
                LOCK.release()
            
            #stampa i risultati finali
            print "RICERCA TERMINATA | n. risultati: %d" % (len(LST))
        #except:
        #    self.killed = True

#thread per la gestione del download
class download(threading.Thread):

    def __init__(self, dtype):
        threading.Thread.__init__(self)
        #dtype indica il tipo di download che si vuole effettuare (getArtist, getAlbum, getBrano)
        self.dtype = dtype
        #si istanzia FileServer per usarne alcuni metodi
        self.fs = FileServer()
    
    def run(self):
        fg = 0
        while(fg == 0):
            try:
                k = raw_input()
                if int(k) <len(LST):
                    fg = 1
                    dinfo = LST[int(k)-1]
                    print dinfo['result'][int(k)-1][0]
                    if os.path.exists(os.path.join(SHARED_DIRECTORY, dinfo['result'][int(k)-1][0])):
                        print "%s %s presente; consentita la sincronizzazione" % (DTYPE[self.dtype], dinfo['result'][int(k)-1][0])
                        fg = 0
                    else:
                        print "download di", (dinfo['result'][0][0]), "iniziato"
                        obj = Pyro.core.getProxyForURI("PYROLOC://%s:%s/fileserver" % (dinfo['address'], dinfo['port']))
                        starttime = time.time()
                        while 1:
                            myid = obj.getID()
                            if myid != ELECTION:
                                break
                            print "waiting for hostlist"
                            time.sleep(2)
                        #il nome del file compresso che verra' trasferito e'composto da
                        #id del client + tempo corrente
                        tarfilename = str(myid) + str(time.time()) + ".tar"

                        try:
                            size = obj.openFile(dinfo['result'][0][0], DTYPE[self.dtype], tarfilename)
                        except IOError,x:
                            print "error: ", x
                        else:
                            print "Filesize=", size
                            total = 0
                            tmppath = os.path.join(SHARED_DIRECTORY, tarfilename)
                            fd = open(tmppath, "wb")
                            while True:
                                chunk = obj.retrieveNextChunk()
                                sys.stdout.write(".")
                                sys.stdout.flush()
                                if chunk:
                                    fd.write(chunk)
                                    total+=len(chunk)
                                else:
                                    break
                            obj.closeFile()
                            fd.close()
                            duration = time.time() - starttime
                            op = "tar -C %s -xf %s" % (SHARED_DIRECTORY, tmppath.replace(" ", "\ "))
                            os.system(op)
                            os.system("rm " + tmppath)
                            if DTYPE[self.dtype] != "artist":
                                #si effettua un controllo su artist.obj per verificare che l'attributo
                                #albums corrisponda con gli album effettivamente presenti
                                #l'operazione e' necessaria nel caso in cui non si voglia scaricare l'intero artista
                                albumpath = os.sep.join(os.path.join(SHARED_DIRECTORY, dinfo['result'][0][0]).split(os.sep)[:-1])
                                self.fs.checkAlbumsForArtist(albumpath)
                                
                            print "-"*78
                            print "File trasferito:", dinfo['result'][0][0]
                            print "Byte ricevuti:", total
                            print "Tempo impiegato:", duration, "secondi"
                            print "Media:", int(total/duration/1024.0), "kb/sec"
                            print "-"*78
                else:
                    print "NUMERO DIGITATO NON CORRISPONDENTE"
            except ValueError:
                if k == "Q":
                    fg = 1
                    showMenu(2)

#def musyncClient(argv):
def musyncClient(**kwargs):
    Pyro.core.initClient()
    os.system(['clear','cls'][os.name == 'nt'])

    print "RICERCA DI TIPO", kwargs["searchtype"]
    if DTYPE[kwargs["searchtype"]] == "artist":
        print "ARTISTA", kwargs["artist"]
    if DTYPE[kwargs["searchtype"]] == "album":
        print "ARTISTA", kwargs["artist"]
        print "ALBUM", kwargs["album"]
    print "Digita il numero corrispondente per avviare il download"
    print "Digita Q per tornare al menu download/sincronizzazione"
    print "_"*78

    #per ogni host della hostlist, avvia la ricerca
    args = kwargs.copy()
    args.pop("searchtype")
    obj = Pyro.core.getProxyForURI("PYROLOC://%s:%s/fileserver" % (Pyro.core.Daemon().hostname, PORT))
    while 1:
        hostlist = obj.getHostListForClient()
        if hostlist != ELECTION:
            break
        print "waiting for hostlist"
        time.sleep(2)

    for host in hostlist.keys():
        thread = doSearch(hostlist[host], BUFFER, kwargs["searchtype"], **args)
        thread.start()

    #avvia il thread per mostrare i risultati
    sthread = showSearch(len(hostlist))
    sthread.start()

    dthread = download(kwargs["searchtype"])
    dthread.start()

    #attende il termine della computazione
    dthread.join()

