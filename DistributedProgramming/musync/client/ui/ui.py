import sys
import os
from musync.client.ui import musyncClient, showMenu, FILEMANAGER_ACTIONS

def musyncUI(argv):
    os.system(['clear','cls'][os.name == 'nt'])
    fg = 0
    showMenu(1)
    while fg == 0:
        input = raw_input("COMANDO_")
        if input == "Q":
            fg = 1
        elif input == str(1):
            showMenu(3)
            fg2 = 0
            while fg2 == 0:
                dinput = raw_input("COMANDO_")
                if dinput == "Q":
                    fg2 = 1
                    showMenu(1)
                else:
                    if dinput in FILEMANAGER_ACTIONS.keys():
                        actionlist = FILEMANAGER_ACTIONS[dinput]
                        method = actionlist[-2]
                        resmethod = actionlist[-1]
                        kwargs = {}
                        for param in actionlist[:-2]:
                            kwargs[param[0]] = raw_input(param[1])
                        resmethod(method(**kwargs))
                    else:
                        print "COMANDO NON CORRETTO"
        elif input == str(2):
            showMenu(2)
            fg2 = 0
            while fg2 == 0:
                dinput = raw_input("COMANDO_")
                if dinput == "Q":
                    fg2 = 1
                    showMenu(1)
                elif dinput == str(1):
                    artist = raw_input("ARTISTA_")
                    print "artista, ", artist
                    musyncClient(searchtype = "getArtist", artist=artist)
                elif dinput == str(2):
                    artist = raw_input("ARTISTA (Se vuoto effettua la ricerca dell'album su tutti gli artisti_")
                    album = raw_input("ALBUM_")
                    print "artista, ", artist
                    print "album, ", album
                    musyncClient(searchtype = "getAlbum", artist=artist, album = album)
                else:
                    print "COMANDO NON CORRETTO"
        else:
            print "COMANDO NON CORRETTO"
    

if __name__ == "__main__":
    musyncUI(sys.argv[1:])
