import Pyro
from musync.config import START_ELECTION, DENIED
from Pyro.core import PyroURI
import random

class MusyncNetwork():
    """
        E' il server che mantiene traccia dello stato della rete e  il bs corrente.
        Il server ha una copia della hostlist e si sincronizza ad ogni modifica con
        il bs.
    """
    def __init__(self):
        self.currentbs = "NOBS"
        self.hostlist = {}
        self.status = 'OK'
    
    def accessRequest(self, uri):
        """
            E' il server centrale che fornisce l'accesso al gruppo al primo client
        """
        self.status = 'election'
        idhost = int(random.random()* 100)
        return {idhost:uri}, idhost

    def amIBully(self, uri):
        return self.currentbs == uri

    def event(self, event):
        if event == HOSTLIST_PROPERTY:
            self.hostlist = list(msg)
            print "New HostList: ",event.msg
    
    def getCurrentBS(self):
        if self.status == 'election':
            return ""
        if self.currentbs == 'NOBS':
            self.status = 'election'
        return self.currentbs

    def getHostList(self):
        if self.status == 'election':
            return None
        return self.hostlist

    def bootStrapDown(self):

        print "Checking Bootstrap.."
        try:
            print "BootStrap at: ", self.currentbs, self.currentport
            mnuri = PyroURI (self.currentbs, BS_NAME, self.currentport, "PYROLOC")
            proxy=Pyro.core.getProxyForURI(str(mnuri))
            for i in range(3):
                if  bool(proxy.isUP()):
                    print "BootStrap is UP Tentativo: ", i+1
            return DENIED
        except:
            print "BootStrap is Down"
            return START_ELECTION

    def setCurrentBS(self, uri):
        self.currentbs = uri
        self.status = "OK"
        return True
    
    def updateHostList(self, hostlist):
        self.hostlist = hostlist
        return "OK"
