#!/usr/bin/env python
# -*- coding: utf8 -*- 
import sys
from musync.server import MusyncNetwork
import Pyro

def main(IP,port):
    """
        Avvia  il server Musync Network
    """
    #Musync Server
    Pyro.core.initServer()
    daemon = Pyro.core.Daemon(host=IP, port=int(port))
    # use Delegation approach for object implementation
    obj=Pyro.core.ObjBase()
    obj.delegateTo(MusyncNetwork())
    daemon.connect(obj,'MusyncNetwork')
    print "\nMusync Network Server Started at: %s:%s\n" % (IP,port)
    daemon.requestLoop()

if __name__=="__main__":
    if len(sys.argv) == 3:
        try:
            main(sys.argv[1], sys.argv[2])
        except KeyboardInterrupt:
            print "Bye bye"
            sys.exit()
    else:
        print 
        print "USAGE: IP ADDRESS SERVER MUSYNC - PORT SERVER MUSYNC"
        print 


