from threading import Thread
class AbstractServer(Thread):
    def __init__(self, server=None, kwargs={}):
        Thread.__init__(self)
        self.setDaemon(1)
        self.starter = server
        self.kw = kwargs

    def run(self):
        self.starter.start(**self.kw)

    def waitUntilStarted(self):
        return self.starter.waitUntilStarted()


