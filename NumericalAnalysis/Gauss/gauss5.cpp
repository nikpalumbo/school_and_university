#include <stdlib.h>   
#include <stdio.h>

  /* Dichiarazione Costanti e funzioni*/
#define rig 100
#define col 100
#define max_mat 20
  /*Fine dichiarazioni Costanti e funzioni */

/*Funzione che genera una matrice casuale*/
double genera_matrice(double mat[rig][col],int n)
{ 
  int i,k,temp;
  
  
  for (i=1;i<=n;i++)
  {
   for (k=1;k<=n;k++)
     {
         do
          temp=rand();
         while (temp>max_mat);
         
         mat[i][k]=temp;
         
     }    
  }
 
}  
/*Fine della funzione*/
/*Funzione che genera una matrice casuale e inserisce el vettore dei termini noti la somma
  delle righe in modo da avere le soluzioni sempre uguale [111....1] */
double genera_matrice_pilotata(double mat[rig][col],double vet[rig],int n)
{ 
  int i,k,temp,somma;
  FILE *f;
  f = fopen("gauss.txt", "wt");
  fprintf(f,"Matrice generata\nDimensione = %d\n",n);
  fprintf(f,"Legenda:\n %d %s %d %s ",n,"colonne per la matrice\n",n+1,"esima colonna termini noti\n");
  for (i=1;i<=n;i++)
  {
      somma=0;
   for (k=1;k<=n;k++)
     {
         do
          temp=rand();
         while (temp>max_mat);
         
         mat[i][k]=temp;
         somma=somma+temp;
         fprintf(f,"%+7.6Le\t",mat[i][k]);
     }    
     vet[i]=somma;
     fprintf(f,"%s %+20.4Le\t %+30.9Le\t\n","*",vet[i]);
  }
   fclose(f);
}  
/*Fine della funzione*/
/*Funzione che genera una matrice casuale e inserisce el vettore dei termini noti la somma
  delle righe in modo da avere le soluzioni sempre uguale [111....1] */
double stampa_matrice_file(double mat[rig][rig],double vet[rig],double vetsol[rig],int n,int ipiv[rig])
{ 
  int i,k,temp,somma;   
  FILE *f;
  f = fopen("gauss.txt", "a");
  fprintf(f,"Matrice calcolata con il metodo di gauss\nDimensione = %d\n",n);
  fprintf(f,"Legenda:\n %d %s %d %s %d %s %d %s",n,"colonne per la matrice\n",n+1,"esima colonna temine noto\n",n+2,"esima colonna rappresenta le soluzioni\n",n+3,"esima colona rappresenta l'errore\n\n\n");
 
  for (i=1;i<=n;i++)
   {
       for (k=1;k<=n;k++)
         {
          fprintf(f,"%+15.6Le\t",mat[ipiv[i]][k]);
         }    
          fprintf(f,"%s %+20.4Le\t %+30.9Le\t %20.9Le\t\n","*",vet[ipiv[i]],vetsol[ipiv[i]],1-vetsol[ipiv[i]]);
         }     
     fprintf(f,"\n");
     fclose(f);   
}  
/*Fine della funzione*/
/*Funzione che acquisisce una matrice di dimensione n */
double acquisisci_matrice(double mat[rig][col],int n)
{ 
  int i,k;   
  for (i=1;i<=n;i++)
  {
   for (k=1;k<=n;k++)
     {
         printf("\nInserisci l'elemento di riga %d %s %d %s",i," colonna ",k," :");
         
         scanf("%lf",&mat[i][k]);
     }    
  }
}  
/*Fine della funzione*/
/*Funzione che acquisice un vettore */
double acquisisci_vettore(double vet[rig],int n)
{ 
  int i;   
  for (i=1;i<=n;i++)
  {
         printf("\nInserisci %d %s",i," elemento dei termini noti :");
         scanf("%lf",&vet[i]);      
  }

}  
/*Fine della funzione*/
    
   

/*Funzione che genera un vettore casuale*/
double genera_vettore(double vet[rig],int n)
{ 
  int i,temp;   
  for (i=1;i<=n;i++)
  {
         do
          temp=rand();
         while (temp>max_mat);
   vet[i]=temp;      
  }

}  
/*Fine della funzione*/
    

/*Funzione che stampa una matrice */
void stampa_matrice(double mat[rig][col],int n)
{
    int i,k;
   for (i=1;i<=n;i++)
     {
       for (k=1;k<=n;k++)
        {
         printf("%lf\t",mat[i][k]);
        }    
    printf("\n"); 
     }    
}
/*Fine della funzione*/
/*Funzione che stampa una matrice secondo l'ordine di righe del vettore ipiv*/
void stampa_matrice_ipiv(double mat[rig][col],int n,int ipiv[rig])
{
    int i,k;
   for (i=1;i<=n;i++)
     {
       for (k=1;k<=n;k++)
        {
         printf("%lf\t",mat[ipiv[i]][k]);
        }    
    printf("\n"); 
     }    
}
/*Fine della funzione*/


/*Funzione che stampa un vettore*/
void stampa_vettore(double vet[rig],int n)
{
    int i;
    double temp;
    for (i=1;i<=n;i++)
     {
         temp=vet[i];
         printf("%lf\t",vet[i]);
     }    
   printf("\n"); 
}
/*Fine della funzione*/
/*Funzione che stampa il vettore delle soluzioni nel modo segno mantissa esponente*/
void stampa_vettore_soluzioni(double vet[rig],int ipiv[rig],int n)
{
    int i;
    double temp;
    for (i=1;i<=n;i++)
     {
         temp=vet[i];
         printf("%.20lf\t",vet[ipiv[i]]);
     }    
   printf("\n"); 
}
/*Fine della funzione*/
/*Funzione che stampa un vettore */
void stampa_vettore_ipiv(int vet[rig],int n)
{
    int i;
     for (i=1;i<=n;i++)
     {
         printf("%d\t",vet[i]);
     }    
   printf("\n"); 
}
/*Fine della funzione*/

double valore_assoluto(double a)
{
    double temp1;
    if(a<=0)
    {
     temp1=a*(-1);
    }
    else
    {
      temp1=a;
    }  
    return temp1;    
}    
/*Fine della funzione*/

/* Funzione pivoting effettua il pivoting parziale lungo le righe 
   del sistema assegnato*/
double pivoting (double mat[rig][rig],double vet[rig],int n,int k,int ipiv[rig])
{
    int i,j,r,t;
    double mas,temp;
    
    temp=mat[ipiv[k]][k];
    mas=valore_assoluto(temp);
    r=k;
    for(i=k+1;i<=n;i++)
    {
         temp=mat[ipiv[i]][k];
        if(valore_assoluto(temp)>mas)
        {
            temp=mat[ipiv[i]][k];
            mas=valore_assoluto(temp);
            r=i;
        }    
    }    
    if(r!=k)
    {
        for(j=k;j<=n;j++)
        {
            /*t=mat[r][j];
            mat[r][j]=mat[k][j];
            mat[k][j]=t;*/
            t=ipiv[r];
            ipiv[r]=ipiv[k];
            ipiv[k]=t;
        }    
        /*t=ipiv[r];
        /*ipiv[r]
        /*t=vet[r];
        vet[r]=vet[k];
        vet[k]=z;*/
    }    
}
/* Fine della funzione*/


/*Funzione gauss tasforma il sistema assegnato in uno equivalente 
  triangolare superiore equivalente*/
double gauss(double mat[rig][rig],double vet[rig],int n,int ipiv[rig])
{
    int i,j,k;
    double temp;
    for (k=1;k<=n-1;k++)
    {
      pivoting(mat,vet,n,k,ipiv);  
        for (i=k+1;i<=n;i++)
        {
            temp=(mat[ipiv[i]][k]/mat[ipiv[k]][k]);
            mat[ipiv[i]][k]=0;
            for (j=k+1;j<=n;j++)
            {
                mat[ipiv[i]][j]=mat[ipiv[i]][j]-(temp*mat[ipiv[k]][j]);
            }    
            vet[ipiv[i]]=vet[ipiv[i]]-(temp*vet[ipiv[k]]);
        }    
    }    
}    
/*Fine della funzione*/

/* Funzione valore_assoluto restituisce il valore assoluto 
   di un double dato in input*/


/* Funzione backsubstitution risolve un sistema lineare superiore*/
double back(double Matrice[rig][col],double vet[rig],double vetsol[rig],int n,bool &comp,int ipiv[rig])
{
int i,j;
double r;

comp=true;
if (Matrice[ipiv[n]][n]!=0)
    vetsol[ipiv[n]]=vet[ipiv[n]]/Matrice[ipiv[n]][n];
    else
    if (vet[ipiv[n]]==0)
        {
        vetsol[ipiv[n]]=0;
        comp=false;
        }
        else
        comp=false;
    i=n-1;
while ((i!=0) && comp)
    {
    r=vet[ipiv[i]];
    for (j=i+1;j<=n;j++)
         r=r-(Matrice[ipiv[i]][j]*vetsol[ipiv[j]]);
    if (Matrice[ipiv[i]][i]!=0)
        vetsol[ipiv[i]]=r/Matrice[ipiv[i]][i];
        else
        if (r==0)
                {
                vetsol[ipiv[i]]=0;
                comp=false;
                }
                else
                comp=false;
                    
    i=i-1;
    }
}
/* Fine della funzione*/    

void inizializza_ipiv(int ipiv[rig],int n)
{
    int i;
    for(i=1;i<=n;i++)
    {
        ipiv[i]=i;
    }    
}    

/* Main Program */
int main()
{
    /* Dichiarazione delle Variabili*/
    double a[rig][col];
    double b[rig],x[rig];
    int ipiv[rig];
    int i,k,n,exit;
    bool comp,esci;
    int risp;
    double temp;
    FILE *fp;
   /*Fine dichiarazioni Variabili */
do
{
printf("           RISOLUZIONE DI UN SISTEMA LIENARE CON IL METODO DI GAUSS\n");   
printf("\nInserire la dimensione della matrice : ");
scanf("%d",&n);

do
{ 
printf("\n1)Carica Matrice casuale\n2)Carica Matrice\n\nFai la scelta :");
scanf("%d",&risp);
}
while((risp!=1)&(risp!=2));
if(risp==1)
{
genera_matrice_pilotata(a,b,n);

}
else
{   
     acquisisci_matrice(a,n);
     acquisisci_vettore(b,n);     
}
inizializza_ipiv(ipiv,n);
gauss(a,b,n,ipiv);
back(a,b,x,n,comp,ipiv);
if (comp)
{
    printf("\nil sistema e' determinato, vedi il file  'gauss.txt' prodotto dal programma\nper verificare le soluzioni e gli eventuali errori");
   stampa_matrice_file(a,b,x,n,ipiv);
}
else
{
    printf("\n il sistema e' indeterminato o incompatibile\n");
}
printf("\n1)Esci\nPremi un tasto per continuare...");
scanf("%d",&exit);
}
while(exit!=1);
}
/*Fine Programma Principale*/
