/*
  Name: NICOLA 
  Surname : PALUMBO
  Date: 20/01/05 
  Description: Esercitazione di laboratorio N.3
  calcolo di un integrale mediante un algoritmo adattativo locale
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>


////STRUTTURA DELLE PILE///////////
typedef struct NODO
          {
              double valore;
              NODO *punt;
          };
////FINE STRUTTURA //////////////////////////////////////////////////

////STRUTTURA PER I PUNTATORI AL NODO///////////
typedef struct type_stack
          {
              NODO *top;
          };
////FINE STRUTTURA //////////////////////////////////////////////////


////DICHAIRAZIONE DELLE PROCEDURE///////     

//FUNZIONE VISUALIZZA MAIN////
int opzioni();

//FUNZIONI CREA INSERISCI E CANCELLA NODO////
void make_pila(type_stack *stack);
void make_nodo (double Oggetto , NODO *node);
void del_nodo (NODO *nodo);
void pop (type_stack *stack);
void push (double oggetto,type_stack *stack);
double TopItem(type_stack stack);
//FUNZIONI PER LE PILE FINE//////////////////

//FUNZIONE CHE CALCOLA L'INTEGRALE MEDIANTE UN ALG. LOCALE
void quadlocal(double estr_a, double estr_b, double tol,int funzione,int maxval, 
            double eps, double *Val_int, double *Err,int *iflag,int *conta );
            
//FUNZIONE TRAPEZOIDALE)/////////////////
void trapez(double estr_a,double estr_b, double *q1,int funzione);

//FUNZIONE CALCOLA EPSYLON MACCHINA/////
void eps (double *u);

//FUNZIONE SQRT(X-2)///////////////////
double radice(double x);

//FUNZIONE pol(X)/////////////////////
double pol(double x);

//FUNZIONE |SEN(X)| //////////////////
double seno(double x);

//FUNZIONE |COS(X)/X| ///////////////
double coseno(double x);

//FUNZIONE 3X+1 /////////////////////
double lineare(double x);

////FINE DICHAIRAZIONE DELLE PROCEDURE////

////////////////////////////MAIN PROGRAM///////////////////////////////////////
int main(void)
{
    double a,b,tol,integ,err,epsM;
    int funscelta,conta;
    int maxval, iflag;
do{    
    //ACQUISIZIONE SCELTA DELLA FUNZIONE///////////////////////////////////////
    funscelta=opzioni()-48;
    //FINE ACQUISIZIONE SCELTA FUNZIONE/////////////////////////////////////////
    
    //ACQUISIZIONE DEI DATI////////////////////////////////////////////////////
    printf("**************************************************************\n");
    printf("Inserisci l'estremo sinistro dell'intervallo di integrazione : ");
    scanf("%lf",&a);
    printf("Inserisci l'estremo destro dell'intervallo di integrazione : ");
    scanf("%lf",&b);
    printf("Inserisci la tolleranza  : ");
    scanf("%lf",&tol);
    printf("Inserisci il numero massimo di valutazioni : ");
    scanf("%d",&maxval);
    //FINE ACQUISIZIONE DEI DATI///////////////////////////////////////////////
    
    //CALCOLO DEL VALORE DELL'INTEGRALE////////////////////////////////////////
    eps (&epsM);    
    quadlocal(a,b,tol,funscelta,maxval,epsM,&integ,&err,&iflag,&conta );
    //FINE CALCOLO DEL VALORE DELL'INTEGRALE///////////////////////////////////

    //VISUALIZZAZIONE DEI RISULTATI OTTENUTI///////////////////////////////////
    if(iflag==0)
    {printf("Il risultato :%.10lf \n",integ);
     printf("L'errore : %e\n",err);
     printf("Valutazioni effettuate : %d\n ",conta);}
    else
    {printf("ATTENZIONE!!\n");
    printf("Non e' possible stimare il valore dell'integrale\n con i paramentri" 
    " inseriti\n");}
    system("PAUSE");
    printf("**************************************************************\n");
}    
while(funscelta!=0);    
system("PAUSE");
return 0;
}    
////////////////////////////FINE MAIN PROGRAM///////////////////////////////////

//FUNZIONI PER LE PILE////////////////////////////////////
void make_pila(type_stack *stack)
{(*stack).top = NULL;}

void make_nodo (double Oggetto ,NODO *nodo)
{(*nodo).valore = Oggetto;
 (*nodo).punt = NULL;}

void del_nodo (NODO *nodo)
{if (nodo != NULL)
     {free(nodo);
      nodo = NULL;}
}

void push (double oggetto,type_stack *stack)
{
    NODO *temp;
    temp = (NODO *)malloc(sizeof(NODO));
    make_nodo( oggetto , temp ); 
    (*temp).punt = (*stack).top;
    (*stack).top = temp;
}

void pop (type_stack *stack)
{
    NODO *temp;
    if ((*stack).top != NULL)
      {temp = (*stack).top;
       (*stack).top = (* temp).punt;
       del_nodo (temp);}
}

double TopItem(type_stack stack)
{
    if (stack.top != NULL)
      return((*stack.top).valore);
    else
      return 0;
}
//FINE FUNZIONI PILE////////////////////////////////////

//CALCOLO DELL'AREA DELLA FUNZIONE SCELTA//////////////////////////////////////
void trapez(double estr_a , double estr_b , double *q1, int funzione)
{
    switch (funzione){
        case 1:
          *q1 = ((pol(estr_a) + pol(estr_b))*(estr_b - estr_a))/2;          
          break;
        case 2:  
          *q1 = ((radice(estr_a) + radice(estr_b))*(estr_b - estr_a))/2;
           break;
        case 3:
          *q1 = ((seno(estr_a) + seno(estr_b))*(estr_b - estr_a))/2;
          break;
        case 4:
          *q1 = ((coseno(estr_a) + coseno(estr_b))*(estr_b - estr_a))/2;
          break;
        case 5:
          *q1 = ((lineare(estr_a) + lineare(estr_b))*(estr_b - estr_a))/2;
          break; 
    }    
}
//FINE CALCOLO DELL'AREA DELLA FUNZIONE SCELTA//////////////////////////////////

//CALCOLO DEL VALORE DELL'INTEGRALE MEDIANTE ALG.LOCALE////////////////////////
void quadlocal(double estr_a, double estr_b, double tol,int funzione,int maxval, 
            double eps, double *Val_int, double *Err,int *iflag,int *conta)
{
 double q1,q2,locerr,inf,sup,res,newres;
 type_stack pila1,pila2,pila3;
  make_pila(&pila1);
 make_pila(&pila2);
 make_pila(&pila3);
 *Val_int = 0;
 *Err = 0; 
 trapez(estr_a,estr_b,&q1,funzione);
 *conta = 2;
 push(estr_a,&pila1);
 push(estr_b,&pila2);
 push(q1,&pila3);
 do {
     inf = TopItem(pila1);
     sup = TopItem(pila2);
     res = TopItem(pila3);
     pop(&pila1);
     pop(&pila2);
     pop(&pila3);
     trapez(inf,(inf+sup)/2,&q1,funzione);
     trapez((inf+sup)/2,sup,&q2,funzione);
     newres = q1 + q2;
     locerr = fabs(res - newres)/3;
     if (locerr<(tol*(sup - inf))/(estr_b-estr_a) || ((sup - inf) <= eps*sup))
      {
        *Val_int = *Val_int + newres;
        *Err = *Err + locerr;
      }
     else
      {
        push((inf + sup)/2,&pila1);
        push(sup,&pila2);
        push(q2,&pila3);
        push(inf,&pila1);
        push((inf + sup)/2,&pila2);
        push(q1,&pila3);
      }
     *conta = *conta + 4;
    } while ((pila1.top != NULL) && (*conta <= maxval));
    if (pila1.top == NULL)
      *iflag = 0;
    else
      *iflag = 1;
}
//FINE CALCOLO DEL VALORE DELL'INTEGRALE MEDIANTE ALG.LOCALE///////////////////


//CALCOLO DELL'EPSYLON MACCHINA////
void eps (double *u)
{
    double u1,u2;
    u1=1;
    do {
        *u=u1;
        u1=u1/2;
        u2=u1+1;
    } while (u2 != 1);
}          
//CALCOLO DELL'EPSYLON MACCHINA/////

//CALCOLO DEI VALORI DELLE FUNZIONI///////////    
double pol(double x)
{return ((x*x)+3);}

double radice(double x)
{return (sqrt(x-2));}

double seno(double x)
{return (1/x);}

double coseno(double x)
{return (fabs(cos(x)/x));}

double lineare(double x)
{return (3*x+1);}
///FINE CALCOLO DEI VALORI DELLE FUNZIONI///////////

////VISUALIZZA IL MENU DI SCELTA///////////////
///RESTITUISCE LA SCELTA FATTA DALL'UTENTE////
int opzioni()
{
    char scelta[10];
    bool esci=false;
    int c;
    do 
    {
    system("cls");
    printf("ESERCITAZIONE N.3------>CALCOLO NUMERICO\n\n\n");
    printf("      1)       Funzione : x^2+3  \n");
    printf("      2)       Funzione : sqrt(x-2)\n");
    printf("      3)       Funzione : |sin(x)|\n");    
    printf("      4)       Funzione : |cos(x)/x|\n");
    printf("      5)       Funzione : 3x+1\n"); 
    printf("      0)       Esci\n\n\n");
    printf("Fai la scelta :");
    scanf("%s",&scelta);
    if(scelta[0]>='0' && scelta[0]<='5')
    {
      if (scelta[0]=='0')  
       {exit(c);}//fine del programma
      else
        return(scelta[0]);
    }    
    else
    {esci=false;
     printf("Scelta errata!!!\n\n\n");
     system("PAUSE");}
    }    
    while(!esci);
}    
///FINE MENU////////////////////
                                       
