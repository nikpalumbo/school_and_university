/*
  Name: Nicola  Palumbo
  Copydimht: 
  Author: Nicola Palumbo
  Date: 09/12/04 14.52
  Description: Esercitazione N�3
*/

#include <stdio.h>
#include <stdlib.h>
/******************************Dichiarazioni delle Costanti***************************************/
#define dim 100
/******************************FINE Dichiarazioni************************************************/

/********************************Dichiarazioni delle procedure***********************************/
void diffdivise(float x[dim],float[dim],int n);
void diffdivise2(float x[dim],float[dim],int n);
void stampa_vettore(float x[dim],float y[dim],int n);
void carica_vettore(float x[dim],float y[dim],int n,int i);
void horner_newton(int n,float x[dim], float y[dim], float punto,float &output);
void aggiunta_nodi(int n,float x[dim], float y[dim],int addn,int &error);
void azzera_vettori(int n,float x[dim],float y[dim]);
void stampa_coefficienti(float y[dim],int n);
bool controllo_ascisse(float x[dim],int n);
int opzioni();
/********************************Fine Dichiarazioni delle procedure***********************************/


/********************************Programma Principale**************************************************/
int main()
{
    
/********************************Dichiarazioni delle Variabili*****************************************/
    int n=0,addn,error=0,i,nval;
    float x[dim],y[dim],puntival[dim];
    float punto,output,interv=0;
    static bool coefficienti,caricamento,esci=false;
    int opt,scelta;
    FILE *f;
/********************************Fine Dichiarazioni delle Variabili************************************/
do
{
printf("                       CALCOLO NUMERICO ESERCIZATIONE N 2\n\n");
opt=opzioni()-48;
switch (opt)
{
    case 1 :  
        
    printf("Inserisci il numero dei nodi :");
    scanf("%d",&n);  
    if(n!=0)
      {carica_vettore(x,y,n,1);
      if (controllo_ascisse(x,n))
       {printf("\n\nLe coordinate inserite :\n\n");
        stampa_vettore(x,y,n);
        caricamento=true;}
      else
          {azzera_vettori(n,x,y);
          coefficienti=false;
          caricamento=false;
         printf("Le ascisse dei nodi inseriti non sono corretti\n\n\n");}
      }
    break;  
    
    case 2 :  
      
      if(caricamento==true)  
       {diffdivise(x,y,n);  
        printf("\n\nI coefficeinti del polinomio sono :\n"); 
        stampa_coefficienti(y,n);
        coefficienti=true;}
      else
       {printf("Devi prima inserire i nodi\n\n\n");}
       break;
      
    case 3 :
           
      if(coefficienti==true)
      {//printf("Inserisci il punto in cui vuoi valutare il polinomio :");
       //scanf("%f",&punto);
       printf("In quanti punti vuoi valutare il polinomio : ");
       scanf("%d",&nval);
       f=fopen("Newton.txt","w");
       interv=-0.2;
       for (i=1;i<=nval;i++)
        {  
            
            puntival[i]=interv+0.2;
            interv=interv+0.2;
            fprintf(f,"%f    ",interv);    }
         fprintf(f,"\n");
       for (i=1;i<=nval;i++)
       {horner_newton(n,x,y,puntival[i],output);
       fprintf(f,"%f  ",output);}}            
      else
       {printf("Devi calcolare prima i coefficeinti del polinomio\n\n\n");}
       fclose(f);
       break;
       
    case 4 :
        
      if(coefficienti==true && caricamento==true)
      {printf("Inserisci il numero dei nodi che vuoi aggiungere :");
       scanf("%d",&addn);
       aggiunta_nodi(n,x,y,addn,error);
       if(error!=1)
        {n=n+addn;//mi ricordo che il polinomio non � + formato da n nodi ma da n+ i nodi aggiunti 
         stampa_coefficienti(y,n);}
       else
         {azzera_vettori(n,x,y);
          coefficienti=false;
          caricamento=false;
          printf("Le ascisse dei nodi inseriti non sono corretti\n\n\n");}  
      }
      else
       {printf("\nDevi calcolare i coefficeinti del polinomio\n\n\n");}
      break;
      
    case 5 :
          azzera_vettori(n,x,y);
          coefficienti=false;
          caricamento=false;
          printf("I vettori e le varibili sono state azzerate\nadesso puoi inserire dei nuvoi nodi\n");
      break;
          
    
          
    default: break;         
}
}
while(!opt==0);

}
/***********************************Caricamento delle coordinate*********************************/
void carica_vettore(float x[dim],float y[dim],int n,int i)
{
    int j;        
    for (j=i;j<=n;j++)
         {
          printf("\nInserisci %d %s",j,"i termini della x :");
          scanf("%f",&x[j]);      
          printf("\nInserisci %d %s",j,"i termini della y :");
          scanf("%f",&y[j]);      
         }
}
/***********************************Fine Caricamento delle coordinate*********************************/ 

/***********************************Stampa coorfinate*********************************************/    
void stampa_vettore(float x[dim],float y[dim],int n)
{
    int i;
    for (i=1;i<=n;i++)
     {
         printf("%s %f %s %f\n","x",x[i],"y",y[i]);
     }    
   printf("\n"); 
}
/***********************************Fine Stampa coorfinate*********************************************/    

/**********************************Procedure Controllo Ascisse**********************************/
bool controllo_ascisse(float x[dim],int n)
{ int k=0,i=0;
while(++i<=n)
 {
  while(++k<=n)
   {
     if(x[k]==x[i] && i!=k)
     {return false;}
     
   }
 }
 return true;
}





/**********************************Fine Procedure Controllo Ascisse******************************/
/**********************************Procedure differenze divise**********************************/


void diffdivise(float x[dim],float y[dim],int n)
{
    int i,k=1;
    
     for(k=1;k<=n-1;k++)
      {
       for(i=n;i>=k+1;i--)
        {y[i]=(y[i]-y[i-1])/(x[i]-x[i-k]);}//schema Aitek    
      }
    
    
}
/**********************************Fine Procedure differenze divise********************************/

void diffdivise2(float x[dim],float y[dim],int n)
{   int k=1;
     for(k=1;k<n;k++)
        {y[n]=(y[n]-y[k])/(x[n]-x[k]);}                 
          
}


/*********************************Procedure Horner Newton***************************************/
void horner_newton(int n,float x[dim], float y[dim], float punto,float &output)
{
    int i=1;
    output=y[n];
    for(i=n-1;i>=1;i--)
    {
        output=output*(punto-x[i])+y[i];
    }    
}    

/*********************************Fine Procedure Horner Newton***********************************/
void aggiunta_nodi(int n,float x[dim], float y[dim],int addn,int &error)
{
    int k=1;//variabile in cui viene memorizzato il numero dei nodi che si vuol aggiungere
    
    carica_vettore(x,y,n+addn,n+1);
    if (controllo_ascisse(x,n+addn))
    {
       for(k=1;k<=addn;k++)
          {diffdivise2(x,y,n+k);}
    }
    else
    error=1;    
    
}    
void azzera_vettori(int n,float x[dim],float y[dim])
{
    int i;
    for(i=1;i<=n;i++)
    {
        x[i]=0;
        y[i]=0;
    }    
}    
/************************************MENU PRINCIPALE*********************************************/
int opzioni()
{
    char scelta[100];
    bool esci=false;
    int c;
    do 
    {
    printf("1) Inserisci nodi\n");    
    printf("2) Calcolo dei coefficienti\n");
    printf("3) Calcola il valore del polinomio in un punto\n");
    printf("4) Aggiunta di nuovi nodi\n");
    printf("5) Azzera vettori\n");
    printf("0) Esci\n\n\n");
    printf("Fai la scelta :");
    scanf("%s",&scelta);
    if(scelta[0]>='0' && scelta[0]<='5')
    {
      if (scelta[0]=='0')  
       {exit(c);}//fine del programma
      else
        return(scelta[0]);
    }    
    else
    {esci=false;
     printf("Scelta errata!!!\n\n\n");}
    }    
    while(!esci);
}    
void stampa_coefficienti(float y[dim],int n)
{
    int i;
    for (i=1;i<=n;i++)
     {
         printf("a%d %s %f\n",i-1,"=",y[i]);
     }    
   
}







