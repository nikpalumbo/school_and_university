/*
  Name: NICOLA 
  Surname : PALUMBO
  Date: 20/01/05 
  Description: Esercitazione di laboratorio N.4
  Calcolo di una approssimazione dello zero di una 
  funzione mediante il metodo di Dekker Brent
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>


////DICHAIRAZIONE DELLE PROCEDURE/////// 
void dekker_brent(double &a,double &b,int maxval,double &xdb,double &valxdb,double eps,
char scelta,double tol,double tolf,int &ifail);
void eps (double *u);
double f(double x,int caso);
int opzioni();
////FINE DICHAIRAZIONE DELLE PROCEDURE////


////////////////////////////MAIN PROGRAM///////////////////////////////////////
int main(void)
{    
    int funscelta=0,maxval=0,ifail=0;
    double a,b,tol1,tol2,epsM,fa,fb;
    double xdb,fxdb;
do{    
    //ACQUISIZIONE SCELTA DELLA FUNZIONE///////////////////////////////////////
    funscelta=opzioni()-48;
    //FINE ACQUISIZIONE SCELTA FUNZIONE/////////////////////////////////////////
    
    //ACQUISIZIONE DEI DATI////////////////////////////////////////////////////
    printf("**************************************************************\n");
    printf("Inserisci l'estremo sinistro dell'intervallo : ");
    scanf("%lf",&a);
    printf("Inserisci l'estremo destro dell'intervallo di integrazione : ");
    scanf("%lf",&b);
    fa=f(a,funscelta);
    fb=f(b,funscelta);
    printf("Inserisci la tolleranza (stima dello zero della funzione) : ");
    scanf("%lf",&tol1);
    printf("Inserisci la tolleranza (stima del valore di f(x)) : ");
    scanf("%lf",&tol2);
    printf("Inserisci il numero massimo di valutazioni : ");
    scanf("%d",&maxval);
    //FINE ACQUISIZIONE DEI DATI///////////////////////////////////////////////
    eps(&epsM);
    dekker_brent(a,b,maxval,xdb,fxdb,epsM,funscelta,tol1,tol2,ifail);
    //VISUALIZZAZIONE DEI RISULTATI OTTENUTI///////////////////////////////////
    if(ifail!=1) 
    {printf(" il valore di xdb : %e\n",xdb);
    printf("il valore di fxdb  : %e\n",fxdb);}
    else
    printf("ATTENZIONE NON E' STATO POSSIBILE DETERMINARE UNA SOLUZIONE DI f(x)\n");
    printf("**************************************************************\n");
    system("PAUSE");
}    
while(funscelta!=0);    
system("PAUSE");
return 0;
}    
////////////////////////////FINE MAIN PROGRAM///////////////////////////////////

void dekker_brent(double &a,double &b,int maxval,double &xdb,double &valxdb,double eps,
char scelta,double tol,double tolf,int &ifail)
{
double y2,y1,y0,x0,x1,d,f0,f1;
int k;

x0=a;
x1=b;
y2=x0;
y1=y2+eps*fabs(a);
y0=y1;
f0=f(x0,scelta);
f1=f(x1,scelta);
k=1;
while ((((fabs(x1-y2))>(tol*fabs(x1))) || ((fabs(f1))>tolf)) && (k<maxval))
{
    if (y2!=y0)
    {
        d=f1*(x1-y1)/(f1-f0);
        if ((d*(x1-y2)<0)||(fabs(d)>fabs(x1-y2)))
        {
            d=(x1-y2)/2;
        }
    }
    else
        d=(x1-y2)/2;
    x0=x1;
    f0=f1;
    x1=x1-d;
    f1=f(x1,scelta);
    y0=y1;
    y1=y2;
    if (f0*f1<0)
    {
        y2=x0;
    }
    k++;
}
xdb=x1;
valxdb=f1;
if (k>=maxval)
ifail=1;
}

double f(double x,int caso)
{
switch (caso)
{
    case 1 :  
            return ((3*(x*x*x*x))-(11*(x*x*x))-(21*(x*x))+(99*x)-54);
    case 2 :
            return ((x*x)-2-log(x));
    case 3 :
            return log(x);
    case 4 : 
            return (exp(x)+x);
    case 5 :
            return ((3*x)+2);
    case 6 : 
            return ((x+3)/(x-1/2));
    default :
            return 0; 
} 
}
//CALCOLO DELL'EPSYLON MACCHINA////
void eps (double *u)
{
    double u1,u2;
    u1=1;
    do {
        *u=u1;
        u1=u1/2;
        u2=u1+1;
    } while (u2 != 1);
}          
//CALCOLO DELL'EPSYLON MACCHINA/////

////VISUALIZZA IL MENU DI SCELTA///////////////
///RESTITUISCE LA SCELTA FATTA DALL'UTENTE////
int opzioni()
{
    char scelta[10];
    bool esci=false;
    int c;
    do 
    {
    system("cls");
    printf("ESERCITAZIONE N.4------>CALCOLO NUMERICO\n");
    printf("PALUMBO NICOLA 566 / 1424 \n\n\n");
    printf("      1)       Funzione : 3x^4 - 11x^3 - 21x^2 + 99x - 54\n\n");
    printf("      2)       Funzione : x^2 - 2 - log(x)\n\n");
    printf("      3)       Funzione : log(x)\n\n");    
    printf("      4)       Funzione : e^x + x\n\n");
    printf("      5)       Funzione : 3x + 2\n\n");
    printf("      6)       Funzione : (x + 3)/ ( x - 1/2)\n\n");
    printf("      0)       Esci\n\n\n");
    printf("Fai la scelta :");
    scanf("%s",&scelta);
    if(scelta[0]>='0' && scelta[0]<='6')
    {
      if (scelta[0]=='0')  
       {exit(c);}//fine del programma
      else
        return(scelta[0]);
    }    
    else
    {esci=false;
     printf("Scelta errata!!!\n\n\n");
     system("PAUSE");}
    }    
    while(!esci);
}    
///FINE MENU////////////////////
                                       
